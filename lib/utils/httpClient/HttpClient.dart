import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:kafaat_loyalty/screens/onBoarding/ui/OnBoardingPage.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/VariablesUtils.dart';
import 'package:kafaat_loyalty/utils/cahce/CacheManager.dart';
import 'package:kafaat_loyalty/utils/httpClient/data/ApiExceptionsData.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class HttpClient {
  HttpClient() {
    if (UserCache.isTokenExist()) VariablesUtils.headers['Authorization'] = 'Bearer  ${UserCache.getToken()}';
  }

  final http.Response _timeoutResponse = http.Response('timeout', 800);

  Future<ApiResponseHandler> get<T>({required String endPoint, Map<String, dynamic>? params}) async {
    try {
      final response = await http
          .get(Uri.parse('${vUtils.baseUrl}$endPoint'), headers: VariablesUtils.headers)
          .timeout(const Duration(seconds: 30), onTimeout: () => _returnResponse(response: _timeoutResponse));

      return _returnResponse(response: response) as dynamic;
    } catch (ex) {
      log(ex.toString());
      return _returnResponse(response: http.Response('', 5200));
    }
  }

  Future<ApiResponseHandler> post<T>({required String endPoint, dynamic payload, bool isJson = true}) async {
    try {
      print(payload);
      final response = await http
          .post(Uri.parse(vUtils.baseUrl + endPoint), headers: VariablesUtils.headers, body: isJson ? jsonEncode(payload) : payload)
          .timeout(const Duration(seconds: 30), onTimeout: () => _returnResponse(response: _timeoutResponse));

      return _returnResponse(response: response);
    } on SocketException catch (ex) {
      log(ex.message);
      return _returnResponse(response: http.Response('', 5200));
    }
  }

  Future<ApiResponseHandler> multiPart<T>({required String endPoint, Map<String, String>? payload, List<File>? files}) async {
    try {
      final request = await http.MultipartRequest('POST', Uri.parse(vUtils.baseUrl + endPoint));
      request.headers['Authorization'] = VariablesUtils.headers['Authorization']!;
      request.fields.addAll(payload!);

      files?.forEach((file) async {
        request.files.add(await http.MultipartFile.fromPath('formFile', file.path));
      });

      var result = await request.send();
      var fromStream = await http.Response.fromStream(result);

      print(request.files);
      log('${fromStream.statusCode.toString()}: ${fromStream.body}');

      return _returnResponse(response: fromStream);
    } on SocketException catch (ex) {
      return _returnResponse(response: http.Response('', 5200));
    }
  }

  Future<dynamic> put<T>({required String endPoint, dynamic payload}) async {
    try {
      final response = await http
          .put(Uri.parse(vUtils.baseUrl + endPoint), headers: {'Content-Type': 'application/json'}, body: jsonEncode(payload))
          .timeout(const Duration(seconds: 30), onTimeout: () => _returnResponse(response: _timeoutResponse));

      return _returnResponse(response: response);
    } on SocketException catch (ex) {
      return _returnResponse(response: http.Response('', 5200));
    }
  }

  Future<ApiResponseHandler> delete<T>({required String endPoint, dynamic payload}) async {
    try {
      final response = await http
          .delete(Uri.parse(vUtils.baseUrl + endPoint), headers: VariablesUtils.headers, body: jsonEncode(payload))
          .timeout(const Duration(seconds: 30), onTimeout: () => _returnResponse(response: _timeoutResponse));

      return _returnResponse(response: response);
    } on SocketException {
      return _returnResponse(response: http.Response('', 5200));
    }
  }

  dynamic _returnResponse({required http.Response response}) {
    log('____________________________________________________');
    log('${response.request?.headers}');
    log('${response.request?.url.toString()}: ${response.statusCode.toString()}');
    log(response.body);
    log('____________________________________________________');

    switch (response.statusCode) {
      case HttpStatus.ok:
      case HttpStatus.created:
      case HttpStatus.accepted:
      case HttpStatus.noContent:
        return ApiResponseHandler(response.statusCode, response.body.isEmpty ? '{}' : response.body);
      case HttpStatus.unauthorized:
      case HttpStatus.forbidden:
        if (response.request!.url.toString().contains('/api/authenticate')) {
          var jsonData = jsonDecode(response.body);
          return ApiResponseHandler(
            response.statusCode,
            ApiException(
              response.statusCode,
              jsonData['detail'] ?? jsonData['title'] ?? response.body ?? AppStrings.unexpectedError,
              jsonData['detail'] ?? jsonData['title'] ?? response.body ?? AppStrings.unexpectedError,
              () => null,
            ),
          );
        }
        nService.pushAndRemove(const OnBoardingPage());
        break;
      case HttpStatus.conflict:
        return ApiResponseHandler(
          response.statusCode,
          ApiException(response.statusCode, response.body, response.body, () => null),
        );
      default:
        try {
          var jsonData = jsonDecode(response.body);
          return ApiResponseHandler(
            response.statusCode,
            ApiException(
              response.statusCode,
              jsonData['detail'] ?? jsonData['title'] ?? response.body ?? AppStrings.unexpectedError,
              jsonData['detail'] ?? jsonData['title'] ?? response.body ?? AppStrings.unexpectedError,
              () => null,
            ),
          );
        } catch (ex) {
          return ApiResponseHandler(-1, ApiException(-1, AppStrings.unexpectedError, AppStrings.unexpectedError, () => null));
        }
    }
  }
}

HttpClient httpClient = HttpClient();
