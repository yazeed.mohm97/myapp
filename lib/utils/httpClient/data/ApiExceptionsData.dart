import 'dart:io';

import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ApiExceptionsData {
  static Map<int, ApiException> exceptions = {
    HttpStatus.notFound: ApiException(HttpStatus.notFound, 'page not found !', 'page not found !', () {}),
    HttpStatus.badRequest: ApiException(HttpStatus.badRequest, 'bad request !', 'bad request !', () {}),
    HttpStatus.internalServerError:
        ApiException(HttpStatus.internalServerError, 'internal server error !', 'internal server error !', () {}),
    HttpStatus.forbidden: ApiException(HttpStatus.unauthorized, 'session expired', 'unauthorized user !', () {}),
    HttpStatus.unauthorized: ApiException(HttpStatus.unauthorized, 'session expired', 'unauthorized user !', () {}),
    409: ApiException(409, 'Email not valid, make sure the company register in kafaat',
        'Email not valid, make sure the company register in kafaat', () {}),
    800: ApiException(800, 'Request timeout', 'Request timeout', () {}),
    5100: ApiException(5100, 'Session expired', 'generalTechnicalError', () {}),
    5200: ApiException(5200, 'Unexpected Error Occurred', 'unexpectedErrorOccurred', () {}),
  };
}

ApiExceptionsData apiExceptionsData = ApiExceptionsData();
