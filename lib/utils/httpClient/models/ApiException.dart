/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ApiException {
  late int statusCode;
  late String businessError;
  late String technicalError;
  late Function() action;

  ApiException(this.statusCode, this.businessError, this.technicalError, this.action);

  Map<String, String> toJson() => {
        'statusCode': statusCode.toString(),
        'businessError': businessError,
        'technicalError': technicalError,
      };
}
