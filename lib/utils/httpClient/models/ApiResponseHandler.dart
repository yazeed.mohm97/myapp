import 'dart:io';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ApiResponseHandler {
  int statusCode;
  dynamic response;

  ApiResponseHandler(this.statusCode, this.response);
}
