import 'package:flutter/material.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MethodsUtils {
  ///hide keyboard if it's opened
  hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
}

MethodsUtils mUtils = MethodsUtils();
