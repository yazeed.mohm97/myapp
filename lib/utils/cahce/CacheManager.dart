import 'package:kafaat_loyalty/utils/VariablesUtils.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class CacheManager {
  static bool isExist(String key) => VariablesUtils.prefs.get(key) != null;
}

class UserCache {
  static bool isTokenExist() => VariablesUtils.prefs.getString('token') != null;

  static void setToken(String token) async => await VariablesUtils.prefs.setString('token', token);

  static String? getToken() => VariablesUtils.prefs.getString('token');

  static void removeToken() async => await VariablesUtils.prefs.remove('token');
}
