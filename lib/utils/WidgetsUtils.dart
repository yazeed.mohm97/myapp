import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class WidgetsUtils {
  static snackBar(BuildContext context, String text, {Color? bgColor}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: TextWidget(text: text, color: AppColors.white), backgroundColor: bgColor),
    );
  }
}
