import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class AppNotifications {
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  initMessaging(BuildContext context) async {
    subscribeToTopic('deem_all_email_users');
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: true,
      criticalAlert: true,
      provisional: true,
      sound: true,
    );

    await messaging.setForegroundNotificationPresentationOptions(alert: true, badge: true, sound: true);

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      //User granted permission;
    } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
      //User granted provisional permission
    } else {
      //User declined or has not accepted permission
    }

    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description: 'This channel is used for high importance Deem mail notification',
      importance: Importance.max,
    );

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');

    const DarwinInitializationSettings initializationSettingsIOS = DarwinInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
      // onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );

    const InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: (response) => _handleForegroundNotificationTap(context, response),
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );

    AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
      channel.id,
      channel.id,
      channelDescription: channel.description,
      importance: Importance.max,
      priority: Priority.high,
      ticker: 'ticker',
    );
    NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);

    // Handle receiving notification in foreground (app is opened)
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;

      // TODO: change payload to support notification types other than email

      // show notification
      if (notification != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          message.notification?.body,
          platformChannelSpecifics,
          // payload: message.data,
        );
      }
    });

    //{"message":{"topic":"test_gmail.com","notification":{"body":"test notification"},"data":{"type":"null","email":"\"test@gmail.com\""}}}

    // Handle receiving background and terminated notification
    RemoteMessage? initialMessage = await FirebaseMessaging.instance.getInitialMessage();

    // Terminated notification
    if (initialMessage != null && context.mounted) {
      _handleBackgroundNotificationTap(context, message: initialMessage);
    }

    // Background notification
    FirebaseMessaging.onMessageOpenedApp
        .listen((backgroundMessage) => _handleBackgroundNotificationTap(context, message: backgroundMessage));
  }

  // TODO: change navigation to screens other than mail if supported
  _handleForegroundNotificationTap(BuildContext context, NotificationResponse response) async {
    if (response.payload != null) {
      // log("#### Handling foreground notification with email id: ${response.payload}");
    }
  }

  _handleBackgroundNotificationTap(BuildContext context, {RemoteMessage? message}) {
    if (message != null) {
      // log("#### Handling background notification with message: ${message.data}");
    }
  }

  subscribeToTopic(String topic) async {
    String topicName = topic.contains('@') ? topic.replaceAll('@', '_') : topic;
    await messaging.subscribeToTopic(topicName);
  }

  unsubscribeFromTopic(String topic) async {
    String topicName = topic.contains('@') ? topic.replaceAll('@', '_') : topic;
    await messaging.unsubscribeFromTopic(topicName);
  }
}
