import 'package:shared_preferences/shared_preferences.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class VariablesUtils {
  final baseUrl = 'https://www.example.com/api/';
  static late SharedPreferences prefs;
  static String? userMail;

  static Map<String, String> headers = {
    'Accept': 'application/json; charset=UTF-8',
    'Content-Type': 'application/json',
    'Accept-Language': '',
    'Authorization': '',
  };
}

VariablesUtils vUtils = VariablesUtils();
