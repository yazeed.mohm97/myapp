import 'package:flutter/material.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class NavigationService {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  pushAndRemove(Widget widget, {BuildContext? context}) => Navigator.pushAndRemoveUntil(
      context ?? navigatorKey.currentContext!, MaterialPageRoute(builder: (ctx) => widget), (route) => false);

  pushWithBack(Widget widget, {BuildContext? context}) => Navigator.push(
        context ?? navigatorKey.currentContext!,
        MaterialPageRoute(builder: (ctx) => widget),
      );
}

NavigationService nService = NavigationService();
