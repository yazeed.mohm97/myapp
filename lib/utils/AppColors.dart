import 'package:flutter/material.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class AppColors {
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color brown = Color(0xFFB18758);
  static const Color brownAE = Color(0xFFD4BDAE);
  static const Color green7F = Color(0xFF748E7F);
  static const Color grey0D = Color(0x1100000D);
  static const Color grey34 = Color(0x6A000034);
  static const Color greyBF = Color(0xFFC2C1BF);
  static const Color greyCE = Color(0xFFCECECE);
  static const Color grey8F = Color(0xFF8F8F8F);
  static const Color grey80 = Color(0x8F8F8F80);
  static const Color blackLight = Color(0xFF18292D);
  static const Color orangeLight = Color(0xFFF7A491);
  static const Color orange = Color(0xFFF47458);
  static const Color orangeDark = Color(0xFFF47257);
  static const Color red = Color(0xFFFF2C00);
  static const Color blue = Colors.blue;
}
