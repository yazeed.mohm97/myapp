import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/HomePage.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/onBoarding/ui/OnBoardingPage.dart';
import 'package:kafaat_loyalty/screens/profile/providers/ProfileProvider.dart';
import 'package:kafaat_loyalty/screens/profile/ui/widgets/DeleteAccountDialog.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/cahce/CacheManager.dart';
import 'package:kafaat_loyalty/utils/constants/AppTextStyles.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BackgroundWidget extends StatelessWidget {
  const BackgroundWidget({
    Key? key,
    this.title = '',
    required this.child,
    this.showBack = true,
    this.showLanguage = true,
    this.onBack,
    this.showTitle = true,
    this.showLogout = false,
    this.resizeToAvoidBottomInset = false,
    this.topSafeArea = true,
  }) : super(key: key);
  final String title;
  final Widget child;
  final bool showBack;
  final bool showLanguage;
  final Function()? onBack;
  final bool showTitle;
  final bool showLogout;
  final bool resizeToAvoidBottomInset;
  final bool topSafeArea;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SvgPicture.asset(
                'assets/images/header.svg',
                height: 136.h,
                width: 783.w,
                fit: BoxFit.fitHeight,
                color: AppColors.brown,
              ),
              Transform.translate(
                offset: Offset(0, 15.h),
                child: SvgPicture.asset(
                  'assets/images/footer.svg',
                  height: 176.26.h,
                  width: 1014.64.w,
                  fit: BoxFit.fitHeight,
                  color: AppColors.brown,
                  alignment: const Alignment(0.7, -100),
                ),
              ),
            ],
          ),
          SafeArea(
            top: topSafeArea,
            bottom: false,
            child: Padding(
              padding: const EdgeInsets.only(top: kToolbarHeight),
              child: child,
            ),
          ),
          SafeArea(
            child: SizedBox(
              height: kToolbarHeight,
              child: Align(
                alignment: AlignmentDirectional.topCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (showBack)
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: BackButton(
                          onPressed: () => onBack?.call() ?? Provider.of<MainPageProvider>(context, listen: false).back(),
                        ),
                      ),
                    // if (showLanguage)
                    //   InkWell(
                    //     child: const Padding(
                    //       padding: EdgeInsets.all(8.0),
                    //       child: Icon(Icons.translate),
                    //     ),
                    //     onTap: () async {
                    //       await context.setLocale(
                    //         context.locale.languageCode.toLowerCase() == 'ar' ? const Locale('en') : const Locale('ar'),
                    //       );
                    //     },
                    //   ),
                    if (showTitle &&
                        context.watch<MainPageProvider>().backstack.isNotEmpty &&
                        context.watch<MainPageProvider>().backstack.last is! HomePage)
                      Expanded(child: Center(child: TextWidget(text: title, style: AppTextStyles.title, align: TextAlign.center))),
                    if (showLogout)
                      PopupMenuButton<int>(
                        itemBuilder: (context) => [
                          PopupMenuItem(
                            value: 1,
                            child: ListTile(
                              leading: const Icon(Icons.logout_outlined),
                              title: TextWidget(text: AppStrings.logout),
                              minLeadingWidth: 0,
                              contentPadding: EdgeInsets.zero,
                              onTap: () {
                                Navigator.pop(context);
                                Provider.of<ProfileProvider>(context, listen: false).removeProfile();
                                Provider.of<MainPageProvider>(context, listen: false).reset();
                                nService.pushAndRemove(const OnBoardingPage());
                              },
                            ),
                          ),
                          PopupMenuItem(
                            value: 1,
                            child: ListTile(
                              leading: const Icon(Icons.delete_outline, color: AppColors.red),
                              title: TextWidget(text: AppStrings.deleteAccount),
                              minLeadingWidth: 0,
                              contentPadding: EdgeInsets.zero,
                              onTap: () {
                                Navigator.pop(context);
                                showDialog(context: context, builder: (context) => const DeleteAccountDialog());
                              },
                            ),
                          ),
                        ],
                        onSelected: (value) {},
                        offset: const Offset(0, kToolbarHeight - 10),
                        elevation: 2,
                      )

                    // IconButton(
                    //   icon: SvgPicture.asset(AppIcons.logout, colorFilter: const ColorFilter.mode(AppColors.brown, BlendMode.dstIn)),
                    //   onPressed: () {
                    //     UserCache.removeToken();
                    //     Provider.of<ProfileProvider>(context, listen: false).removeProfile();
                    //     nService.pushAndRemove(const OnBoardingPage());
                    //   },
                    // )
                    else
                      const SizedBox(),
                    if (!showLogout) SizedBox(width: 60.w),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
