import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class RetryWidget extends StatelessWidget {
  const RetryWidget({Key? key, this.message, this.action}) : super(key: key);
  final String? message;
  final Function()? action;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 100,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextWidget(text: message ?? 'Error occurred'),
            SizedBox(height: 12.h),
            InkWell(
              onTap: () => action?.call(),
              child: TextWidget(text: 'Retry', color: AppColors.brown, size: 18.sp),
            ),
          ],
        ),
      ),
    );
  }
}
