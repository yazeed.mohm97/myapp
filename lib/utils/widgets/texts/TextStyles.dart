import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class TextStyles {
  static TextStyle purpleHeadline = TextStyle(color: AppColors.brown, fontSize: 26.sp, fontWeight: FontWeight.w500);
}
