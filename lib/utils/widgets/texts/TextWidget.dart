import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class TextWidget extends StatelessWidget {
  const TextWidget({
    required this.text,
    this.maxLines,
    this.size,
    this.color,
    this.fontWeight,
    this.height,
    this.align,
    this.direction,
    this.softWrap = true,
    this.underline = false,
    this.lineThrough = false,
    this.overflow,
    this.style,
    Key? key,
  }) : super(key: key);

  final String text;
  final int? maxLines;
  final double? size;
  final Color? color;
  final FontWeight? fontWeight;
  final double? height;
  final TextAlign? align;
  final TextDirection? direction;
  final bool softWrap;
  final bool underline;
  final bool lineThrough;
  final TextOverflow? overflow;
  final TextStyle? style;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      textDirection: direction,
      softWrap: softWrap,
      overflow: overflow,
      maxLines: maxLines,
      style: style ??
          TextStyle(
            color: color ?? AppColors.black,
            fontSize: size,
            fontWeight: fontWeight,
            height: height,
            decoration: underline
                ? TextDecoration.underline
                : lineThrough
                    ? TextDecoration.lineThrough
                    : TextDecoration.none,
          ),
    );
  }
}
