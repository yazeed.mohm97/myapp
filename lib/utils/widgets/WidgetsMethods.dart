import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class WidgetsMethods {
  showCustomDialog(BuildContext context, Widget dialog) {
    showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: AppColors.black.withOpacity(0.9),
      builder: (context) => dialog,
    );
  }
}

WidgetsMethods wUtils = WidgetsMethods();
