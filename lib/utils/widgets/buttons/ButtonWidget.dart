import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({
    Key? key,
    required this.onTap,
    this.text,
    this.height,
    this.width,
    this.radius,
    this.margin = EdgeInsets.zero,
    this.padding = EdgeInsets.zero,
    this.bgColor,
    this.borderColor,
    this.textColor,
    this.textSize,
    this.fontWeight,
    this.translate = false,
    this.child,
  }) : super(key: key);
  final Function() onTap;
  final double? height;
  final double? width;
  final double? radius;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final Color? bgColor;
  final Color? borderColor;
  final String? text;
  final Color? textColor;
  final double? textSize;
  final FontWeight? fontWeight;
  final bool translate;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      padding: padding,
      child: InkWell(
        onTap: () => onTap.call(),
        child: Container(
          height: height ?? 52.h,
          width: width ?? 300.w,
          decoration: BoxDecoration(
            color: bgColor ?? AppColors.brown,
            borderRadius: BorderRadius.circular(radius ?? 4),
            border: Border.all(color: borderColor ?? Colors.transparent),
          ),
          child: Center(
            child: child ??
                TextWidget(
                  text: translate ? text!.tr() : text!,
                  color: textColor ?? AppColors.white,
                  size: textSize,
                  fontWeight: fontWeight,
                ),
          ),
        ),
      ),
    );
  }
}
