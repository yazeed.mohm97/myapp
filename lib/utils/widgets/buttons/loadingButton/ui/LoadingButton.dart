import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class LoadingButton extends StatelessWidget {
  const LoadingButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.height,
    this.width,
    this.radius,
    this.margin = EdgeInsets.zero,
    this.padding = EdgeInsets.zero,
    this.bgColor,
    this.textColor,
    this.textSize,
    this.fontWeight,
    this.translate = false,
    this.usedProvider,
  }) : super(key: key);

  final Function() onTap;
  final double? height;
  final double? width;
  final double? radius;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final Color? bgColor;
  final String text;
  final Color? textColor;
  final double? textSize;
  final FontWeight? fontWeight;
  final bool translate;
  final Function(LoadingButtonProvider provider)? usedProvider;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => LoadingButtonProvider(),
      child: Selector<LoadingButtonProvider, bool>(
        selector: (context, provider) {
          if (usedProvider != null) usedProvider!(provider);
          return provider.isLoading;
        },
        builder: (context, isLoading, child) {
          return Container(
            margin: margin,
            padding: padding,
            child: InkWell(
              onTap: () => onTap.call(),
              child: Container(
                height: height ?? 52.h,
                width: width ?? 300.w,
                decoration: BoxDecoration(
                  color: bgColor ?? AppColors.brown,
                  borderRadius: BorderRadius.circular(radius ?? 4),
                ),
                child: Center(
                  child: isLoading
                      ? const CircularProgressIndicator(color: AppColors.white)
                      : TextWidget(
                          text: translate ? text.tr() : text,
                          color: textColor ?? AppColors.white,
                          size: textSize,
                          fontWeight: fontWeight,
                        ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
