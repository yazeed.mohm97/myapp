import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class AppTextStyles {
  static TextStyle title = TextStyle(fontSize: 20.sp, color: AppColors.brown, fontWeight: AppFonts.medium);
}
