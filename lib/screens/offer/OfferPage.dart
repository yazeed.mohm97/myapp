import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/bookmarks/providers/BookmarksProvider.dart';
import 'package:kafaat_loyalty/screens/business/ui/BusinessPage.dart';
import 'package:kafaat_loyalty/screens/dialogs/discountCodeDialog/ui/DiscountCodeDialog.dart';
import 'package:kafaat_loyalty/screens/dialogs/QRDialog/ui/QRCodeScannerDialog.dart';
import 'package:kafaat_loyalty/screens/dialogs/pdfViewer/PDFViewerScreen.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/map/ui/MapPage.dart';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/screens/offers/providers/OffersProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/widgets/WidgetsMethods.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/ButtonWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OfferPage extends StatelessWidget {
  const OfferPage({Key? key, required this.offer}) : super(key: key);
  final Offer offer;

  @override
  Widget build(BuildContext context) {
    return OfferScreen(offer: offer);
  }
}

class OfferScreen extends StatefulWidget {
  const OfferScreen({Key? key, required this.offer}) : super(key: key);
  final Offer offer;

  @override
  State<OfferScreen> createState() => _OfferScreenState();
}

class _OfferScreenState extends State<OfferScreen> {
  @override
  void initState() {
    Provider.of<OffersProvider>(context, listen: false).visited(widget.offer.businessId ?? 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bookmarksProvider = Provider.of<BookmarksProvider>(context, listen: false);
    final offersProvider = Provider.of<OffersProvider>(context, listen: false);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 24.h, width: double.infinity),
            Container(
              height: 366.h,
              width: 380.h,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(6.r),
                boxShadow: const [
                  BoxShadow(color: AppColors.grey0D, blurRadius: 16),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      SizedBox(width: 16.w),
                      TextWidget(text: AppStrings.expireOn, size: 12.sp, color: AppColors.grey8F),
                      TextWidget(
                          text: DateFormat('dd, MMMM yyyy').format(widget.offer.expirationDate ?? DateTime.now()),
                          size: 12.sp,
                          color: AppColors.brown),
                      const Spacer(),
                      InkWell(
                        child: Consumer<OffersProvider>(
                          builder: (context, provider, _) {
                            return Container(
                              height: 45.h,
                              width: 132.w,
                              decoration: BoxDecoration(
                                color: widget.offer.bookedMarked ?? false ? AppColors.brown : AppColors.white,
                                borderRadius: BorderRadius.circular(6.r),
                                boxShadow: [
                                  BoxShadow(color: AppColors.grey0D, blurRadius: 60.r),
                                ],
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(AppIcons.bookmark,
                                      color: widget.offer.bookedMarked ?? false ? AppColors.white : AppColors.grey8F),
                                  SizedBox(width: 10.w),
                                  TextWidget(
                                    text: widget.offer.bookedMarked ?? false ? AppStrings.bookmarked : AppStrings.bookmark,
                                    size: 13.sp,
                                    color: widget.offer.bookedMarked ?? false ? AppColors.white : AppColors.grey8F,
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                        onTap: () async {
                          if (!widget.offer.bookedMarked!) {
                            offersProvider.bookmark(widget.offer.id!, true);
                            var aResponse = await bookmarksProvider.addBookMark(widget.offer.id!);
                            offersProvider.bookmark(widget.offer.id!, aResponse?.statusCode == HttpStatus.created);
                          } else {
                            offersProvider.bookmark(widget.offer.id!, false);
                            var dResponse = await bookmarksProvider.deleteBookmarkByOfferId(widget.offer.id!);
                            offersProvider.bookmark(widget.offer.id!, dResponse?.statusCode == HttpStatus.noContent ? false : true);
                          }
                        },
                      ),
                    ],
                  ),
                  Expanded(
                    child: widget.offer.image != null
                        ? CachedNetworkImage(
                            imageUrl: widget.offer.image!.filePath!,
                            placeholder: (context, _) => SvgPicture.asset(AppIcons.image, height: 100, color: AppColors.grey8F),
                            fit: BoxFit.fill,
                            width: double.infinity,
                          )
                        : Center(child: SvgPicture.asset(AppIcons.image, height: 100, color: AppColors.grey8F)),
                  ),
                  const Divider(height: 0),
                  SizedBox(
                    height: 100.h,
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(
                          onTap: widget.offer.code != null
                              ? () => wUtils.showCustomDialog(
                                    context,
                                    DiscountCodeDialog(
                                      offerId: widget.offer.id!,
                                      title: context.locale.languageCode.toLowerCase() == 'ar'
                                          ? widget.offer.title ?? ''
                                          : widget.offer.titleEn ?? '',
                                      subTitle: context.locale.languageCode.toLowerCase() == 'ar'
                                          ? widget.offer.description ?? ''
                                          : widget.offer.descriptionEn ?? '',
                                      offerNo: widget.offer.id.toString(),
                                      amount: widget.offer.percentage.toString(),
                                      code: widget.offer.code ?? '',
                                    ),
                                  )
                              : null,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(AppIcons.ticket, color: widget.offer.code != null ? AppColors.brown : AppColors.greyBF),
                              SizedBox(height: 18.w),
                              TextWidget(
                                  text: AppStrings.viewDiscountCode,
                                  size: 13.sp,
                                  color: widget.offer.code != null ? AppColors.brown : AppColors.greyBF),
                            ],
                          ),
                        ),
                        VerticalDivider(indent: 20.h, endIndent: 20.h),
                        InkWell(
                          onTap: widget.offer.qr != null
                              ? () => wUtils.showCustomDialog(context, QRCodeScannerDialog(offerId: widget.offer.id!))
                              : null,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(AppIcons.scan, color: AppColors.brown),
                              SizedBox(height: 18.w),
                              TextWidget(text: AppStrings.scanQRCode, size: 13.sp, color: AppColors.brown),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            VerticalDivider(indent: 20.h, endIndent: 20.h),
                            InkWell(
                              onTap: widget.offer.details != null
                                  ? () => wUtils.showCustomDialog(context, PDFViewerScreen(filePath: widget.offer.details!.filePath!))
                                  : null,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    AppIcons.document,
                                    color: widget.offer.details != null ? AppColors.brown : AppColors.greyBF,
                                  ),
                                  SizedBox(height: 18.w),
                                  TextWidget(
                                    text: AppStrings.viewDocument,
                                    size: 13.sp,
                                    color: widget.offer.details != null ? AppColors.brown : AppColors.greyBF,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 30.h),
            if (widget.offer.locations != null && widget.offer.locations!.isNotEmpty)
              ButtonWidget(
                onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
                  MainPageItem(
                    -1,
                    widget.offer.businessName ?? AppStrings.map,
                    AppIcons.image,
                    MapPage(locations: widget.offer.locations ?? []),
                    false,
                  ),
                ),
                bgColor: AppColors.white,
                textColor: AppColors.brown,
                borderColor: AppColors.brown,
                radius: 8.r,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(AppIcons.discovery, color: AppColors.brown),
                    SizedBox(width: 8.w),
                    TextWidget(text: AppStrings.viewOnMap, color: AppColors.brown),
                  ],
                ),
              ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 55.h, horizontal: 24.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextWidget(
                      text: context.locale.languageCode.toLowerCase() == 'ar'
                          ? widget.offer.title ?? '(No Name)'
                          : widget.offer.titleEn ?? '(No Name)',
                      color: AppColors.black,
                      size: 18.sp),
                  SizedBox(height: 9.h, width: double.infinity),
                  TextWidget(
                      text: context.locale.languageCode.toLowerCase() == 'ar'
                          ? widget.offer.description ?? ''
                          : widget.offer.descriptionEn ?? '',
                      color: AppColors.grey8F,
                      size: 14.sp),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.brownAE,
        child: SvgPicture.asset(AppIcons.shop),
        onPressed: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
          MainPageItem(-1, widget.offer.businessName ?? '', AppIcons.image, BusinessPage(businessId: widget.offer.businessId!), false),
        ),
      ),
    );
  }
}
