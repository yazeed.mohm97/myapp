import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestDiscounts/LatestDiscountsWidget.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/searchWidget/SearchTextField.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MyDiscountsPage extends StatelessWidget {
  const MyDiscountsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyDiscountsScreen();
  }
}

class MyDiscountsScreen extends StatelessWidget {
  MyDiscountsScreen({Key? key}) : super(key: key);
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 21.w),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 100.h),
            SearchTextField(searchController: searchController),
            SizedBox(height: 29.h),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(
                  categories.length,
                  (index) => Padding(
                    padding: EdgeInsetsDirectional.only(start: 8.w, end: 16.w),
                    child: TextWidget(text: categories[index], color: AppColors.grey8F),
                  ),
                ),
              ),
            ),
            SizedBox(height: 52.h),
            Column(
              children: List.generate(
                categories.length,
                (index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextWidget(text: categories[index], color: AppColors.black, size: 16.sp, fontWeight: AppFonts.medium),
                      SizedBox(height: 9.h),
                      TextWidget(
                        text: AppStrings.checkTheLatestAddedOffers,
                        color: AppColors.grey8F,
                        size: 14.sp,
                        fontWeight: AppFonts.regular,
                      ),
                      SizedBox(height: 22.h),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: List.generate(
                            5,
                            (index) {
                              return Container(
                                height: 180.h,
                                width: 180.w,
                                margin: const EdgeInsets.all(4),
                                decoration: const BoxDecoration(
                                  color: AppColors.white,
                                  boxShadow: [BoxShadow(color: AppColors.grey0D, blurRadius: 24)],
                                ),
                                child: Stack(
                                  children: [
                                    Align(
                                      alignment: AlignmentDirectional.topEnd,
                                      child: Padding(
                                        padding: EdgeInsetsDirectional.only(top: 10.h, end: 10.w),
                                        child: SvgPicture.asset(AppIcons.heart, color: AppColors.black),
                                      ),
                                    ),
                                    Align(
                                      alignment: AlignmentDirectional.topStart,
                                      child: Padding(
                                        padding: EdgeInsetsDirectional.only(top: 8.h),
                                        child: Container(
                                          height: 37.h,
                                          width: 42.w,
                                          decoration: BoxDecoration(
                                            color: AppColors.brown,
                                            borderRadius: BorderRadiusDirectional.only(
                                                topEnd: Radius.circular(6.r), bottomEnd: Radius.circular(6.r)),
                                          ),
                                          child: Center(
                                            child: TextWidget(
                                              text: '40%',
                                              color: AppColors.white,
                                              size: 12.sp,
                                              fontWeight: AppFonts.medium,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Center(child: SvgPicture.asset(AppIcons.image, height: 100))
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 45.h),
                    ],
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  final List<String> categories = ['Food and Drinks', 'Medical', 'Auto', 'Activities', 'Jewellery'];
}
