import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/HomeHeaderWidget.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestDiscounts/LatestDiscountsWidget.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestOffers/LatestOffersWidget.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/myGiftCards/MyGiftCards.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/searchWidget/SearchTextField.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HomeScreen();
  }
}

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
        child: Column(
          children: [
            const HomeHeaderWidget(),
            SizedBox(height: 27.h),
            SearchTextField(searchController: searchController, source: 'HOME'),
            SizedBox(height: 45.h),
            const MyGiftCardsWidget(),
            SizedBox(height: 35.h),
            const LatestOffersWidget(),
            SizedBox(height: 35.h),
            const LatestDiscountsWidget(backScreen: HomePage()),
          ],
        ),
      ),
    );
  }
}
