import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/profile/models/Profile.dart';
import 'package:kafaat_loyalty/screens/profile/providers/ProfileProvider.dart';
import 'package:kafaat_loyalty/screens/profile/ui/ProfilePage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class HomeHeaderWidget extends StatelessWidget {
  const HomeHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<ProfileProvider, ApiResponseHandler?>(
      selector: (context, provider) => provider.response,
      builder: (context, profile, child) {
        if (profile == null) {
          Provider.of<ProfileProvider>(context, listen: false).fetchProfile();
        }
        // else if (profile.statusCode == 200 && profile.response is Profile) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 20.w),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        TextWidget(text: AppStrings.hi, size: 28.sp, fontWeight: AppFonts.light),
                        TextWidget(
                            text: (profile?.response is Profile) ? profile?.response?.firstName ?? '' : '',
                            size: 28.sp,
                            fontWeight: AppFonts.light,
                            color: AppColors.brown),
                      ],
                    ),
                    // SizedBox(height: 4.h),
                    // TextWidget(text: AppStrings.dayAwesome, size: 14.sp, fontWeight: AppFonts.regular, color: AppColors.grey80),
                  ],
                ),
              ),
              InkWell(
                child: Container(
                  height: 58.h,
                  width: 58.w,
                  padding: EdgeInsets.symmetric(vertical: 13.h, horizontal: 13.w),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: const [
                      BoxShadow(color: AppColors.grey0D, blurRadius: 24),
                    ],
                  ),
                  child: (profile?.response is Profile)
                      ? CachedNetworkImage(
                          imageUrl: (profile?.response as Profile).company?.logo?.filePath ?? '',
                          errorWidget: (_, __, ___) => SvgPicture.asset(AppIcons.profileColored),
                        )
                      : SvgPicture.asset(AppIcons.profileColored),
                ),
                onTap: () {
                  Provider.of<MainPageProvider>(context, listen: false).navigate(
                    MainPageItem(-1, AppStrings.myProfile, AppIcons.image, const ProfilePage(), false),
                  );
                },
              ),
            ],
          ),
        );
        // }
        // return SizedBox.shrink();
      },
    );
  }
}
