import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/providers/MyGiftsProvider.dart';
import 'package:kafaat_loyalty/screens/offers/providers/OffersProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class SearchTextField extends StatelessWidget {
  const SearchTextField({Key? key, required this.searchController, this.hint, this.source}) : super(key: key);
  final String? hint;
  final String? source;
  final TextEditingController searchController;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48.h,
      width: 385.w,
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30.r),
        boxShadow: const [
          BoxShadow(
            color: AppColors.grey0D,
            blurRadius: 12,
          ),
        ],
      ),
      child: TextField(
        controller: searchController,
        onChanged: (value) {
          Provider.of<OffersProvider>(context, listen: false).search(value, source: source);

          if (source == 'HOME' || source == 'GIFT') Provider.of<MyGiftsProvider>(context, listen: false).search(value);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hint ?? AppStrings.searchForOffersAndDiscounts,
          hintStyle: TextStyle(color: AppColors.grey8F, fontSize: 14.sp, fontWeight: AppFonts.regular),
          prefixIcon: Align(alignment: AlignmentDirectional.centerStart, child: SvgPicture.asset(AppIcons.search)),
          prefixIconConstraints: BoxConstraints(minHeight: 19.w, maxHeight: 19.w, minWidth: 30.w, maxWidth: 30.w),
        ),
      ),
    );
  }
}
