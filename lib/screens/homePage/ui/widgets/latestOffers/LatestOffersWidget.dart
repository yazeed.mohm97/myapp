import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/HomePage.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestOffers/LatestOfferItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/screens/offers/providers/OffersProvider.dart';
import 'package:kafaat_loyalty/screens/offers/ui/OffersPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/LoadingState.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class LatestOffersWidget extends StatelessWidget {
  const LatestOffersWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 223.h,
      width: double.infinity,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 22.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextWidget(text: AppStrings.latestOffers, color: AppColors.black, size: 16.sp, fontWeight: AppFonts.medium),
                    SizedBox(height: 9.h),
                    TextWidget(
                      text: AppStrings.checkTheLatestAddedOffers,
                      color: AppColors.grey8F,
                      size: 14.sp,
                      fontWeight: AppFonts.regular,
                    ),
                  ],
                ),
                InkWell(
                  child: TextWidget(text: AppStrings.viewAll, color: AppColors.brown, size: 14.sp, underline: true),
                  onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
                    MainPageItem(-1, AppStrings.myOffers, AppIcons.image, const OffersPage(type: 'OFFER'), false),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 19.h),
          Consumer<OffersProvider>(
            builder: (context, provider, _) {
              if (provider.offers == null || provider.offers?.response is LoadingState) {
                if (provider.offers?.response is! LoadingState) provider.getOffers();
                return SizedBox(
                  height: 150.h,
                  child: const Center(child: CircularProgressIndicator(color: AppColors.green7F)),
                );
              } else if (provider.offers?.response is ApiException) {
                return SizedBox(
                  height: 150.h,
                  child: Center(child: TextWidget(text: provider.offers?.response.businessError)),
                );
              } else if (provider.offers?.statusCode == 200 && provider.offers?.response is List<Offers>) {
                return Expanded(
                  child: provider.offersList.isEmpty
                      ? SizedBox(
                          height: 150.h,
                          child: Center(child: TextWidget(text: AppStrings.noItems)),
                        )
                      : ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: provider.homeOffersList.length > 10 ? 10 : provider.homeOffersList.length,
                          itemBuilder: (context, position) => LatestOfferItem(
                            item: provider.homeOffersList[position],
                            backScreen: const HomePage(),
                          ),
                        ),
                );
              }
              return SizedBox(
                height: 150.h,
                child: const Center(child: CircularProgressIndicator(color: AppColors.green7F)),
              );
            },
          ),
        ],
      ),
    );
  }
}
