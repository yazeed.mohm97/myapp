import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/bookmarks/providers/BookmarksProvider.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/offer/OfferPage.dart';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/screens/offers/providers/OffersProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class LatestOfferItem extends StatelessWidget {
  const LatestOfferItem({Key? key, required this.item, required this.backScreen}) : super(key: key);
  final Offer item;
  final Widget backScreen;

  @override
  Widget build(BuildContext context) {
    final offersProvider = Provider.of<OffersProvider>(context, listen: false);
    final bookmarksProvider = Provider.of<BookmarksProvider>(context, listen: false);
    return InkWell(
      child: Container(
        height: 115.h,
        width: 382.w,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 4.w),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(6.r),
          boxShadow: const [
            BoxShadow(color: AppColors.grey0D, blurRadius: 10),
          ],
        ),
        child: Row(
          children: [
            SizedBox(
              height: double.infinity,
              width: ScreenUtil.defaultSize.width / 3,
              child: item.image?.filePath != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(6.r),
                      child: CachedNetworkImage(
                        imageUrl: item.image!.filePath!,
                        placeholder: (context, _) => SvgPicture.asset(AppIcons.image, color: AppColors.black, height: 75.h),
                        fit: BoxFit.fill,
                      ),
                    )
                  : Center(
                      child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 21.w),
                      child: SvgPicture.asset(AppIcons.image, color: AppColors.black, height: 75.h),
                    )),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: AlignmentDirectional.topEnd,
                    child: InkWell(
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(top: 10.h, end: 10.w),
                        child: SvgPicture.asset(item.bookedMarked ?? false ? AppIcons.filledHeart : AppIcons.heart),
                      ),
                      onTap: () async {
                        if (!item.bookedMarked!) {
                          offersProvider.bookmark(item.id!, true);
                          var aResponse = await bookmarksProvider.addBookMark(item.id!);
                          offersProvider.bookmark(item.id!, aResponse?.statusCode == HttpStatus.created);
                          bookmarksProvider.getBookmarks();
                        } else {
                          offersProvider.bookmark(item.id!, false);
                          var dResponse = await bookmarksProvider.deleteBookmarkByOfferId(item.id!);
                          offersProvider.bookmark(item.id!, dResponse?.statusCode == HttpStatus.noContent ? false : true);
                          bookmarksProvider.getBookmarks();
                        }
                      },
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(start: 31.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20.h),
                          TextWidget(
                              text: item.titleText ?? AppStrings.offer,
                              color: AppColors.black,
                              size: 12.sp,
                              fontWeight: AppFonts.medium),
                          SizedBox(height: 10.h),
                          TextWidget(
                            text: item.descriptionText ?? '',
                            color: AppColors.black,
                            size: 10.sp,
                            fontWeight: AppFonts.regular,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
        MainPageItem(-1, item.titleText ?? AppStrings.offers, AppIcons.image, OfferPage(offer: item), false),
      ),
    );
  }
}
