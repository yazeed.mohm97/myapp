import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:kafaat_loyalty/screens/giftCard/ui/GiftCardDetailsPage.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/models/GiftsCards.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MyGiftCardItemWidget extends StatelessWidget {
  const MyGiftCardItemWidget({Key? key, required this.giftCard, required this.backScreen}) : super(key: key);
  final GiftCard giftCard;
  final Widget backScreen;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: 174.h,
        width: 382.w,
        margin: EdgeInsets.symmetric(horizontal: 4.w),
        decoration: BoxDecoration(color: AppColors.brown, borderRadius: BorderRadius.circular(6.r)),
        child: Row(
          children: [
            Container(
              height: double.infinity,
              width: 163.w,
              decoration: BoxDecoration(color: AppColors.brownAE, borderRadius: BorderRadius.circular(6.r)),
              child: giftCard.image != null && giftCard.image?.filePath != null
                  ? ClipRRect(
                      borderRadius: const BorderRadiusDirectional.horizontal(start: Radius.circular(6)),
                      child: CachedNetworkImage(
                        imageUrl: giftCard.image!.filePath!,
                        errorWidget: (context, _, __) => Padding(
                          padding: const EdgeInsets.all(32),
                          child: SvgPicture.asset(AppIcons.image, height: 50.h, color: AppColors.grey8F),
                        ),
                        fit: BoxFit.fill,
                      ),
                    )
                  : Center(child: SvgPicture.asset(AppIcons.image, color: AppColors.white, height: 75.h)),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Align(
                  //   alignment: AlignmentDirectional.topEnd,
                  //   child: Padding(
                  //     padding: EdgeInsetsDirectional.only(top: 10.h, end: 10.w),
                  //     child: SvgPicture.asset(AppIcons.heart, color: AppColors.white),
                  //   ),
                  // ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(top: 50.h, start: 31.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextWidget(
                            text: giftCard.name ?? AppStrings.giftCard,
                            color: AppColors.white,
                            size: 12.sp,
                            fontWeight: AppFonts.medium,
                          ),
                          SizedBox(height: 10.h),
                          TextWidget(
                            text: giftCard.details ?? '',
                            color: AppColors.white,
                            size: 10.sp,
                            fontWeight: AppFonts.regular,
                          ),
                          const Spacer(),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 16.h),
                            child: Align(
                              alignment: AlignmentDirectional.bottomStart,
                              child: TextWidget(
                                text: '*Expire on ${DateFormat('dd, MMMM yyyy').format(giftCard.expirationDate!)}',
                                color: AppColors.white,
                                size: 10.sp,
                                fontWeight: AppFonts.regular,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
        MainPageItem(-1, giftCard.name ?? 'Gift Card', AppIcons.image, GiftCardDetailsPage(giftCard: giftCard), false),
      ),
    );
  }
}
