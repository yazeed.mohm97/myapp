import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/giftCard/ui/GiftCardDetailsPage.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/HomePage.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/myGiftCards/MyGiftCardItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/models/GiftsCards.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/providers/MyGiftsProvider.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/ui/MyGiftCardsPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MyGiftCardsWidget extends StatelessWidget {
  const MyGiftCardsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 213.h,
      width: double.infinity,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 22.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextWidget(text: AppStrings.myGiftCards, color: AppColors.black, size: 16.sp, fontWeight: AppFonts.medium),
                InkWell(
                  child: TextWidget(text: AppStrings.viewAll, color: AppColors.brown, size: 14.sp, underline: true),
                  onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
                    MainPageItem(-1, AppStrings.myGiftCards, AppIcons.image, const MyGiftCardsPage(), false),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 19.h),
          Consumer<MyGiftsProvider>(
            builder: (context, provider, _) {
              if (provider.myGifts == null) {
                Provider.of<MyGiftsProvider>(context, listen: false).getMyGifts();
                return SizedBox(
                  height: 150.h,
                  child: const Center(child: CircularProgressIndicator(color: AppColors.green7F)),
                );
              } else if (provider.myGifts?.response is ApiException) {
                return SizedBox(
                  height: 150.h,
                  child: Center(child: TextWidget(text: provider.myGifts?.response.businessError)),
                );
              } else if (provider.myGifts?.statusCode == 200 && provider.myGifts?.response is List<GiftCard>) {
                return Expanded(
                  child: provider.searchGiftsCards.isEmpty
                      ? SizedBox(height: 150.h, child: Center(child: TextWidget(text: AppStrings.noItems)))
                      : Padding(
                          padding: provider.searchGiftsCards.length == 1
                              ? EdgeInsetsDirectional.only(start: 16.w)
                              : EdgeInsets.zero,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: provider.searchGiftsCards.length,
                            itemBuilder: (context, position) {
                              return MyGiftCardItemWidget(
                                giftCard: provider.searchGiftsCards[position],
                                backScreen: const HomePage(),
                              );
                            },
                          ),
                        ),
                );
              }
              return SizedBox(
                height: 150.h,
                child: const Center(child: CircularProgressIndicator(color: AppColors.green7F)),
              );
            },
          )
        ],
      ),
    );
  }
}
