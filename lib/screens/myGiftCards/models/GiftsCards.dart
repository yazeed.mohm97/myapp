import 'dart:convert';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

List<GiftCard> giftsCardsFromJson(String str) => List<GiftCard>.from(json.decode(str).map((x) => GiftCard.fromJson(x)));

String giftCardToJson(List<GiftCard> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GiftCard {
  GiftCard({
    this.id,
    this.details,
    this.expirationDate,
    this.enabled,
    this.name,
    this.user,
    this.addresses,
    this.attachment,
    this.image,
    this.contact,
  });

  int? id;
  String? details;
  DateTime? expirationDate;
  bool? enabled;
  dynamic name;
  User? user;
  List<Address>? addresses;
  Attachment? attachment;
  Image? image;
  dynamic contact;

  factory GiftCard.fromJson(Map<String, dynamic> json) => GiftCard(
        id: json["id"],
        details: json["details"],
        expirationDate: json["expirationDate"] == null ? null : DateTime.parse(json["expirationDate"]),
        enabled: json["enabled"],
        name: json["name"],
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        addresses: json["addresses"] == null ? [] : List<Address>.from(json["addresses"]!.map((x) => Address.fromJson(x))),
        attachment: json["attachment"],
        image: json["image"] == null ? null : Image.fromJson(json["image"]),
        contact: json["contact"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "details": details,
        "expirationDate":
            "${expirationDate!.year.toString().padLeft(4, '0')}-${expirationDate!.month.toString().padLeft(2, '0')}-${expirationDate!.day.toString().padLeft(2, '0')}",
        "enabled": enabled,
        "name": name,
        "user": user?.toJson(),
        "addresses": addresses == null ? [] : List<dynamic>.from(addresses!.map((x) => x.toJson())),
        "attachment": attachment,
        "image": image?.toJson(),
        "contact": contact,
      };
}

class Address {
  Address({
    this.id,
    this.latitude,
    this.longitude,
    this.name,
    this.nameEn,
    this.generateQr,
    this.qrCode,
    this.businessName,
    this.businessId,
  });

  int? id;
  String? latitude;
  String? longitude;
  dynamic name;
  dynamic nameEn;
  bool? generateQr;
  dynamic qrCode;
  dynamic businessName;
  dynamic businessId;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        name: json["name"],
        nameEn: json["nameEn"],
        generateQr: json["generateQR"],
        qrCode: json["qrCode"],
        businessName: json["businessName"],
        businessId: json["businessId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "name": name,
        "nameEn": nameEn,
        "generateQR": generateQr,
        "qrCode": qrCode,
        "businessName": businessName,
        "businessId": businessId,
      };
}

class Image {
  Image({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}

class User {
  User({
    this.id,
    this.login,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
    this.imageUrl,
    this.activated,
    this.langKey,
    this.createdBy,
    this.createdDate,
    this.lastModifiedBy,
    this.lastModifiedDate,
    this.authorities,
    this.address,
    this.enabled,
    this.gender,
    this.age,
    this.familyAccount,
    this.birthDate,
    this.company,
    this.familyAccounts,
  });

  int? id;
  String? login;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  dynamic imageUrl;
  bool? activated;
  String? langKey;
  String? createdBy;
  DateTime? createdDate;
  String? lastModifiedBy;
  DateTime? lastModifiedDate;
  List<String>? authorities;
  dynamic address;
  bool? enabled;
  String? gender;
  dynamic age;
  bool? familyAccount;
  dynamic birthDate;
  dynamic company;
  dynamic familyAccounts;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        login: json["login"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        email: json["email"],
        phoneNumber: json["phoneNumber"],
        imageUrl: json["imageUrl"],
        activated: json["activated"],
        langKey: json["langKey"],
        createdBy: json["createdBy"],
        createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
        lastModifiedBy: json["lastModifiedBy"],
        lastModifiedDate: json["lastModifiedDate"] == null ? null : DateTime.parse(json["lastModifiedDate"]),
        authorities: json["authorities"] == null ? [] : List<String>.from(json["authorities"]!.map((x) => x)),
        address: json["address"],
        enabled: json["enabled"],
        gender: json["gender"],
        age: json["age"],
        familyAccount: json["familyAccount"],
        birthDate: json["birthDate"],
        company: json["company"],
        familyAccounts: json["familyAccounts"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "login": login,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "phoneNumber": phoneNumber,
        "imageUrl": imageUrl,
        "activated": activated,
        "langKey": langKey,
        "createdBy": createdBy,
        "createdDate": createdDate?.toIso8601String(),
        "lastModifiedBy": lastModifiedBy,
        "lastModifiedDate": lastModifiedDate?.toIso8601String(),
        "authorities": authorities == null ? [] : List<dynamic>.from(authorities!.map((x) => x)),
        "address": address,
        "enabled": enabled,
        "gender": gender,
        "age": age,
        "familyAccount": familyAccount,
        "birthDate": birthDate,
        "company": company,
        "familyAccounts": familyAccounts,
      };
}

class Attachment {
  Attachment({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}
