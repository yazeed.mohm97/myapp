import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/models/GiftsCards.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/repositories/MyGiftsRepository.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MyGiftsProvider extends ChangeNotifier {
  ApiResponseHandler? _myGifts;

  ApiResponseHandler? get myGifts => _myGifts;

  final List<GiftCard> _giftsCards = <GiftCard>[];
  final List<GiftCard> _searchGiftsCards = <GiftCard>[];

  List<GiftCard> get giftsCards => _giftsCards;

  List<GiftCard> get searchGiftsCards => _searchGiftsCards;

  getMyGifts() async {
    _myGifts = await MyGiftsRepository().getMyGifts();
    if (_myGifts?.statusCode == 200 && _myGifts?.response is List<GiftCard>) {
      _giftsCards.clear();
      _searchGiftsCards.clear();
      _giftsCards.addAll(_myGifts?.response);
      _searchGiftsCards.addAll(_myGifts?.response);
    }
    notifyListeners();
  }

  ApiResponseHandler? _giftCard;

  ApiResponseHandler? get giftCard => _giftCard;

  fetchGift(int productId) async {
    _giftCard = await MyGiftsRepository().fetchGiftCard(productId);
    notifyListeners();
  }

  search(String query) {
    if (query.isNotEmpty) {
      _searchGiftsCards.clear();
      _giftsCards.map((element) {
        if (element.name != null && element.name!.contains(query) || element.details != null && element.details!.contains(query)) {
          _searchGiftsCards.add(element);
        }
      }).toList();
    } else {
      _searchGiftsCards.clear();
      _searchGiftsCards.addAll(_myGifts?.response);
    }
    notifyListeners();
  }
}
