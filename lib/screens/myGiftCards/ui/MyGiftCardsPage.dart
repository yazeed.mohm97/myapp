import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/myGiftCards/MyGiftCardItem.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/searchWidget/SearchTextField.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/models/GiftsCards.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/providers/MyGiftsProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/widgets/exceptions/RetryWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MyGiftCardsPage extends StatelessWidget {
  const MyGiftCardsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MyGiftCardsScreen();
  }
}

class MyGiftCardsScreen extends StatefulWidget {
  const MyGiftCardsScreen({Key? key}) : super(key: key);

  @override
  State<MyGiftCardsScreen> createState() => _MyGiftCardsScreenState();
}

class _MyGiftCardsScreenState extends State<MyGiftCardsScreen> {
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    fetchMyGifts();
    super.initState();
  }

  fetchMyGifts() {
    Provider.of<MyGiftsProvider>(context, listen: false).getMyGifts();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 16.h),
        SearchTextField(searchController: searchController, source: 'GIFT'),
        SizedBox(height: 20.h),
        Expanded(
          child: Consumer<MyGiftsProvider>(
            builder: (context, provider, child) {
              if (provider.myGifts == null) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: const Center(child: CircularProgressIndicator(color: AppColors.brown)),
                );
              } else if (provider.myGifts?.response is ApiException) {
                return RetryWidget(
                  message: provider.myGifts?.response.businessError,
                  action: () => fetchMyGifts(),
                );
              } else if (provider.myGifts?.response is List<GiftCard>) {
                return provider.searchGiftsCards.isNotEmpty
                    ? ListView.builder(
                        itemCount: provider.searchGiftsCards.length,
                        itemBuilder: (context, position) {
                          GiftCard myGift = provider.searchGiftsCards[position];
                          return Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 24.w),
                            child: MyGiftCardItemWidget(giftCard: myGift, backScreen: widget),
                          );
                        },
                      )
                    : SizedBox(
                        height: MediaQuery.of(context).size.height / 1.5,
                        child: const Center(child: TextWidget(text: 'No Gifts Cards')),
                      );
              }
              return const SizedBox.shrink();
            },
          ),
        ),
      ],
    );
  }
}
