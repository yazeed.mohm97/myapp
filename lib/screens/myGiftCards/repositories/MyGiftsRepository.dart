import 'package:kafaat_loyalty/screens/myGiftCards/models/GiftsCards.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MyGiftsRepository {
  Future<ApiResponseHandler> getMyGifts() async {
    var response = await HttpClient().get(endPoint: 'gifts');
    if (response.statusCode == 200) {
      response.response = giftsCardsFromJson(response.response);
    }
    return response;
  }

  Future<ApiResponseHandler> fetchGiftCard(int productId) async {
    var response = await HttpClient().get(endPoint: 'gifts/$productId');
    if (response.statusCode == 200) {
      response.response = giftsCardsFromJson(response.response);
    }
    return response;
  }
}
