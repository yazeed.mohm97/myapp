import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class FamilyMemberRepository {
  Future<ApiResponseHandler> addFamilyMember(String email) async {
    var response = await HttpClient().post(endPoint: 'family-user', payload: email, isJson: false);
    return response;
  }

  Future<ApiResponseHandler> deleteFamilyMember(String email) async {
    var response = await HttpClient().delete(endPoint: 'family-user/$email');
    return response;
  }
}
