import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/familyMembers/repositories/FamilyMemberRepository.dart';
import 'package:kafaat_loyalty/screens/profile/providers/ProfileProvider.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class FamilyMembersProvider extends ChangeNotifier {
  final List<String> _members = [];

  List<String> get members => _members;

  ApiResponseHandler? _addResponse;

  ApiResponseHandler? get addResponse => _addResponse;

  Future addMember(String email) async {
    if (!_members.contains(email)) {
      var response = await FamilyMemberRepository().addFamilyMember(email);
      if (response.statusCode == HttpStatus.ok) _members.add(email);
      _addResponse = response;
      notifyListeners();
    }
  }

  setMembers(List<String> value) {
    _members.clear();
    _members.addAll(value);
    // notifyListeners();
  }

  deleteMember(String email) async {
    _members.remove(email);
    notifyListeners();

    var response = await FamilyMemberRepository().deleteFamilyMember(email);
    if (response.statusCode != HttpStatus.ok) {
      _members.add(email);
    } else {
      Provider.of<ProfileProvider>(NavigationService.navigatorKey.currentContext!, listen: false).fetchProfile(forceUpdate: true);
    }
    notifyListeners();
  }
}
