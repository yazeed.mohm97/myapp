import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/familyMembers/providers/FamilyMembersProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/MethodsUtils.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class FamilyMembersPage extends StatelessWidget {
  const FamilyMembersPage({Key? key, required this.membersList}) : super(key: key);
  final List<String> membersList;

  @override
  Widget build(BuildContext context) {
    return FamilyMembersScreen(membersList: membersList);
  }
}

class FamilyMembersScreen extends StatefulWidget {
  const FamilyMembersScreen({Key? key, required this.membersList}) : super(key: key);
  final List<String> membersList;

  @override
  State<FamilyMembersScreen> createState() => _FamilyMembersScreenState();
}

class _FamilyMembersScreenState extends State<FamilyMembersScreen> {
  late TextEditingController _memberEmail;
  late LoadingButtonProvider _buttonProvider;
  bool _deleting = false;

  @override
  void initState() {
    _memberEmail = TextEditingController();
    if (widget.membersList.isNotEmpty) {
      Provider.of<FamilyMembersProvider>(context, listen: false).setMembers(widget.membersList);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<FamilyMembersProvider>(
        builder: (context, provider, child) {
          return Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset(AppIcons.addUser, color: AppColors.brown),
                    Column(
                      children: [
                        TextWidget(
                          text: '${provider.members.length}/8',
                          size: 20.sp,
                          color: AppColors.brown,
                          fontWeight: AppFonts.medium,
                        ),
                        TextWidget(text: AppStrings.remainingInvites, color: AppColors.grey34, size: 14.sp)
                      ],
                    ),
                    const SizedBox(width: 20),
                  ],
                ),
              ),
              SizedBox(height: 53.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 40.w),
                child: TextFormField(
                  controller: _memberEmail,
                  decoration: InputDecoration(
                    label: TextWidget(text: AppStrings.emailAddress, color: AppColors.grey80),
                    prefixIcon: Align(
                      alignment: AlignmentDirectional.centerStart,
                      child: SvgPicture.asset(AppIcons.send, color: AppColors.grey80),
                    ),
                    prefixIconConstraints: BoxConstraints(minHeight: 18.h, maxHeight: 18.h, minWidth: 35.w, maxWidth: 35.w),
                    enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.greyCE)),
                    focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.greyCE)),
                  ),
                ),
              ),
              SizedBox(height: 50.h),
              LoadingButton(
                text: AppStrings.invite,
                radius: 8.r,
                usedProvider: (provider) => _buttonProvider = provider,
                onTap: () async {
                  mUtils.hideKeyboard(context);
                  if (_memberEmail.text.trim().isNotEmpty && !_buttonProvider.isLoading) {
                    _buttonProvider.setLoading(true);
                    await Provider.of<FamilyMembersProvider>(context, listen: false).addMember(_memberEmail.text);
                    _memberEmail.text = '';
                    _buttonProvider.setLoading(false);
                  }
                },
              ),
              SizedBox(height: 59.h),
              Align(
                alignment: AlignmentDirectional.centerStart,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 33.w),
                  child: TextWidget(text: AppStrings.familyMembers, size: 20.sp, color: AppColors.brown, fontWeight: AppFonts.medium),
                ),
              ),
              SizedBox(height: 35.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 33.w),
                child: provider.members.isNotEmpty
                    ? ListView.builder(
                        itemCount: provider.members.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, position) {
                          return SizedBox(
                            width: 360.w,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        TextWidget(
                                            text: provider.members[position].split('@')[0] ?? '',
                                            size: 16.sp,
                                            color: AppColors.brown,
                                            fontWeight: AppFonts.medium),
                                        SizedBox(height: 10.h),
                                        TextWidget(
                                          text: provider.members[position],
                                          size: 12.sp,
                                          color: AppColors.grey80,
                                          fontWeight: AppFonts.medium,
                                        ),
                                      ],
                                    ),
                                    InkWell(
                                      child: Row(
                                        children: [
                                          SvgPicture.asset(AppIcons.delete),
                                          SizedBox(width: 10.h),
                                          TextWidget(text: AppStrings.delete),
                                        ],
                                      ),
                                      onTap: () => Provider.of<FamilyMembersProvider>(context, listen: false).deleteMember(
                                        provider.members[position],
                                      ),
                                    )
                                  ],
                                ),
                                Divider(height: 50.h),
                              ],
                            ),
                          );
                        },
                      )
                    : SizedBox(
                        height: MediaQuery.of(context).size.height * 0.25,
                        child: Center(child: TextWidget(text: AppStrings.noInvitedMembers, color: AppColors.greyBF, size: 16.sp)),
                      ),
              )
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _memberEmail.dispose();
    super.dispose();
  }
}
