import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/bookmarks/ui/BookmarksPage.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/HomePage.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/map/ui/MapPage.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/ui/MyGiftCardsPage.dart';
import 'package:kafaat_loyalty/screens/offers/ui/OffersPage.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/VariablesUtils.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MainPageProvider extends ChangeNotifier {
  String _title = '';

  String get title => _title;

  bool _showBack = false;

  bool get showBack => _showBack;

  ///new router

  final List<Widget> _backstack = [
    const HomePage(),
  ];

  List<Widget> get backstack => _backstack;

  navigate(MainPageItem screen) {
    _title = screen.title;
    _backstack.add(screen.screen);
    _showBack = screen.screen is! HomePage;
    if (screen.index != -1) {
      screensList.firstWhere((element) => element.selected).selected = false;
      screensList.firstWhere((element) => element.screen == screen.screen).selected = true;
    }
    notifyListeners();
  }

  back() {
    if (_backstack.isNotEmpty) _backstack.removeLast();
    _showBack = backstack.isNotEmpty && backstack.last is! HomePage;

    for (var element in screensList) {
      if (_backstack.isNotEmpty && element.screen == _backstack.last) {
        screensList.firstWhere((element) => element.selected).selected = false;
        element.selected = true;
        break;
      }
    }

    notifyListeners();
  }

  reset() {
    _title = '';
    VariablesUtils.headers['Authorization'] = '';
    _backstack.clear();
    _backstack.add(const HomePage());
  }

  final List<MainPageItem> screensList = [
    MainPageItem(0, AppStrings.home, AppIcons.home, const HomePage(), true),
    MainPageItem(1, AppStrings.offers, AppIcons.discount, const OffersPage(type: 'OFFER'), false),
    MainPageItem(2, AppStrings.map, AppIcons.location, const MapPage(), false),
    MainPageItem(3, AppStrings.bookmarks, AppIcons.bookmark, const BookmarksPage(), false),
    MainPageItem(4, AppStrings.giftCards, AppIcons.ticket, const MyGiftCardsPage(), false),
  ];
}
