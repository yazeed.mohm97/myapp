import 'package:flutter/cupertino.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MainPageItem {
  int index;
  String title;
  String icon;
  Widget screen;
  bool selected;

  MainPageItem(this.index, this.title, this.icon, this.screen, this.selected);
}
