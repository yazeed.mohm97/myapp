import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kafaat_loyalty/screens/bookmarks/ui/BookmarksPage.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/HomePage.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/map/controllers/MapController.dart';
import 'package:kafaat_loyalty/screens/map/ui/MapPage.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/ui/MyGiftCardsPage.dart';
import 'package:kafaat_loyalty/screens/offers/ui/OffersPage.dart';
import 'package:kafaat_loyalty/screens/profile/ui/ProfilePage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/VariablesUtils.dart';
import 'package:kafaat_loyalty/utils/configrations/firebase/NotificationsConfig.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MainScreen();
  }
}

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _counter = 0;

  @override
  void initState() {
    AppNotifications()
      ..initMessaging(context)
      ..subscribeToTopic('all_users');
    if (VariablesUtils.userMail != null) AppNotifications().subscribeToTopic(VariablesUtils.userMail!);

    mapController.getLocations();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<MainPageProvider>(context, listen: false);
    if (provider.backstack.isEmpty && context.mounted) {
      provider.navigate(provider.screensList[0]);
    }
    return WillPopScope(
      onWillPop: () async {
        if (context.read<MainPageProvider>().backstack.last is! HomePage) {
          _counter = 0;
          provider.back();
        } else {
          _counter++;
        }
        return _counter > 1;
      },
      child: BackgroundWidget(
        showBack: context.watch<MainPageProvider>().showBack,
        title: context.watch<MainPageProvider>().title,
        showLogout: context.watch<MainPageProvider>().backstack.isNotEmpty && context.watch<MainPageProvider>().backstack.last is ProfilePage,
        showLanguage: context.watch<MainPageProvider>().backstack.isNotEmpty && context.watch<MainPageProvider>().backstack.last is ProfilePage,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: context.watch<MainPageProvider>().backstack.last,
          bottomNavigationBar: Consumer<MainPageProvider>(
            builder: (context, provider, child) {
              return Container(
                height: 80.h + MediaQuery.of(context).padding.bottom,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.vertical(top: Radius.circular(6.r)),
                ),
                child: SafeArea(
                  child: Row(
                    children: List.generate(provider.screensList.length, (position) {
                      MainPageItem item = provider.screensList[position];
                      return Expanded(
                        child: InkWell(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 20.h),
                              SvgPicture.asset(
                                item.icon,
                                colorFilter: ColorFilter.mode(item.selected ? AppColors.brown : AppColors.grey8F, BlendMode.srcIn),
                              ),
                              SizedBox(height: 10.h),
                              Expanded(
                                child: TextWidget(
                                  text: item.title,
                                  size: 12.sp,
                                  color: item.selected ? AppColors.brown : AppColors.grey8F,
                                  align: TextAlign.center,
                                ),
                              ),
                              // const Spacer(),
                              if (item.selected)
                                Container(
                                  height: 5.h,
                                  margin: EdgeInsets.symmetric(horizontal: 15.w),
                                  decoration: BoxDecoration(
                                    color: AppColors.brown,
                                    borderRadius: BorderRadius.circular(10.r),
                                  ),
                                ),
                            ],
                          ),
                          onTap: () => provider.navigate(item),
                        ),
                      );
                    }),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
