import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kafaat_loyalty/screens/login/ui/LoginPage.dart';
import 'package:kafaat_loyalty/screens/register/ui/RegisterPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/ButtonWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

class OnBoardingPage extends StatelessWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      showBack: false,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/images/o2e_logo.svg',
                  // height: 176.h,
                  width: 147.w,
                ),
                TextWidget(text: 'offers to Employees', size: 14.sp, fontWeight: AppFonts.regular),
              ],
            ),
          ),
          ButtonWidget(
            text: AppStrings.loginToYourAccount,
            textSize: 14.sp,
            fontWeight: AppFonts.medium,
            radius: 8.r,
            margin: EdgeInsets.only(bottom: 29.h),
            onTap: () => nService.pushWithBack(const LoginPage(), context: context),
          ),
          RichText(
            text: TextSpan(
              text: AppStrings.dontHaveAnAccount,
              style: TextStyle(color: AppColors.black, fontSize: 14.sp, fontWeight: AppFonts.light),
              children: <TextSpan>[
                TextSpan(
                  text: AppStrings.registerNow,
                  recognizer: TapGestureRecognizer()..onTap = () => nService.pushWithBack(const RegisterPage(), context: context),
                  style: TextStyle(
                    color: AppColors.brown,
                    fontSize: 14.sp,
                    fontWeight: AppFonts.medium,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 50.h, width: double.infinity),
        ],
      ),
    );
  }
}
