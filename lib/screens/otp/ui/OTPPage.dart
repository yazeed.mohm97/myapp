import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kafaat_loyalty/screens/login/ui/LoginPage.dart';
import 'package:kafaat_loyalty/screens/mainPage/ui/MainPage.dart';
import 'package:kafaat_loyalty/screens/otp/repository/OTPRepository.dart';
import 'package:kafaat_loyalty/screens/register/ui/RegisterDoneScreen.dart';
import 'package:kafaat_loyalty/screens/register/ui/RegisterPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/MethodsUtils.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/WidgetsUtils.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/ButtonWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextStyles.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OTPPage extends StatelessWidget {
  const OTPPage({Key? key, required this.email}) : super(key: key);
  final String email;

  @override
  Widget build(BuildContext context) {
    return OTPScreen(email: email);
  }
}

class OTPScreen extends StatefulWidget {
  const OTPScreen({Key? key, required this.email}) : super(key: key);
  final String email;

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  late StreamController<ErrorAnimationType> _errorController;

  late TextEditingController _otpController;
  late LoadingButtonProvider _buttonProvider;
  bool _resend = false;

  @override
  void initState() {
    _errorController = StreamController<ErrorAnimationType>();
    _otpController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      onBack: () => Navigator.pop(context),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 100.h),
              TextWidget(text: AppStrings.otpVerification, style: TextStyles.purpleHeadline),
              SizedBox(height: 17.h),
              RichText(
                text: TextSpan(
                  text: AppStrings.weHaveSentOTP,
                  style: const TextStyle(color: AppColors.black, fontWeight: FontWeight.w400),
                  children: <TextSpan>[
                    TextSpan(
                      text: widget.email,
                      style: const TextStyle(
                        color: AppColors.brown,
                        fontWeight: FontWeight.w400,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 100.h),
              PinCodeTextField(
                appContext: context,
                controller: _otpController,
                length: 6,
                obscureText: false,
                animationType: AnimationType.fade,
                mainAxisAlignment: MainAxisAlignment.center,
                keyboardType: TextInputType.number,
                errorAnimationController: _errorController,
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.underline,
                  fieldHeight: 50,
                  fieldWidth: ScreenUtil.defaultSize.width / 8,
                  activeColor: AppColors.brown,
                  inactiveColor: AppColors.grey80,
                  selectedColor: AppColors.brown,
                  selectedFillColor: AppColors.white,
                  activeFillColor: Colors.white,
                  inactiveFillColor: Colors.white,
                  fieldOuterPadding: EdgeInsets.symmetric(horizontal: 4.w),
                ),
                animationDuration: const Duration(milliseconds: 300),
                backgroundColor: AppColors.white,
                enableActiveFill: true,
                onCompleted: (v) => mUtils.hideKeyboard(context),
                onChanged: (value) {},
              ),
              timer(),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: LoadingButton(
          text: AppStrings.continueT,
          radius: 8.r,
          margin: EdgeInsets.only(bottom: 40.h),
          usedProvider: (provider) => _buttonProvider = provider,
          onTap: () async {
            _buttonProvider.setLoading(true);
            await OTPRepository().verifying(widget.email, _otpController.text).then((value) {
              _buttonProvider.setLoading(false);
              if (value) {
                // nService.pushWithBack(LoginPage(email: widget.email), context: context);
                nService.pushWithBack(const RegisterDoneScreen(), context: context);
              } else {
                _errorController.add(ErrorAnimationType.shake);
                WidgetsUtils.snackBar(
                  context,
                  AppStrings.errorOccurred,
                  bgColor: AppColors.red,
                );
              }
            });
          },
        ),
      ),
    );
  }

  Widget timer() {
    return TweenAnimationBuilder<Duration>(
        duration: const Duration(minutes: 1),
        tween: Tween(begin: const Duration(minutes: 2), end: Duration.zero),
        onEnd: () => setState(() => _resend = true),
        builder: (BuildContext context, Duration value, Widget? child) {
          final minutes = value.inMinutes;
          final seconds = value.inSeconds % 60;
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextWidget(
                  text: '${minutes < 10 ? ('0$minutes') : minutes}:${seconds < 10 ? '0$seconds' : seconds}  ',
                  align: TextAlign.center,
                  color: AppColors.grey8F,
                  size: 16,
                ),
                InkWell(
                  onTap: _resend
                      ? () async {
                          await OTPRepository().resendOTP(widget.email).then((value) {
                            if (value) {
                              Fluttertoast.showToast(msg: AppStrings.resent);
                              setState(() {});
                            } else {
                              WidgetsUtils.snackBar(
                                context,
                                AppStrings.errorOccurred,
                                bgColor: AppColors.red,
                              );
                            }
                          });
                        }
                      : null,
                  child: TextWidget(
                    text: AppStrings.resend,
                    align: TextAlign.center,
                    color: _resend ? AppColors.brown : AppColors.greyBF,
                    size: 16,
                  ),
                ),
              ],
            ),
          );
        });
  }
}
