import 'dart:convert';
import 'dart:io';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OTPRepository {
  Future<bool> verifying(String email, String otp) async {
    try {
      var response = await HttpClient().post(endPoint: 'activate', payload: {'email': email, 'key': otp});
      return response.statusCode == HttpStatus.ok;
    } catch (e) {
      return false;
    }
  }

  Future<bool> resendOTP(String email) async {
    try {
      var response = await HttpClient().post(endPoint: 'account/resend-otp', payload: email, isJson: false);
      return response.statusCode == HttpStatus.ok;
    } catch (e) {
      return false;
    }
  }
}
