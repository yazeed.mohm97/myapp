import 'dart:io';

import 'package:kafaat_loyalty/screens/profile/models/Profile.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ProfileRepository {
  Future<ApiResponseHandler> getProfile() async {
    try {
      var response = await HttpClient().get(endPoint: 'account');
      if (response.statusCode == HttpStatus.ok) {
        response.response = profileFromJson(response.response);
      }
      return response;
    } catch (e) {
      return ApiResponseHandler(5100, 'Catch error');
    }
  }

  Future<ApiResponseHandler> updateProfile(Profile profile) async {
    try {
      var response = await HttpClient().post(endPoint: 'account', payload: profile);
      return response;
    } catch (e) {
      return ApiResponseHandler(5100, 'Catch error');
    }
  }

  Future<ApiResponseHandler> deleteAccount(String mail) async {
    try {
      var response = await HttpClient().delete(endPoint: 'account/$mail');
      return response;
    } catch (e) {
      return ApiResponseHandler(5100, 'Catch error');
    }
  }
}
