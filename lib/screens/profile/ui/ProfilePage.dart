import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/familyMembers/ui/FamilyMembersPage.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/profile/models/Gender.dart';
import 'package:kafaat_loyalty/screens/profile/models/Profile.dart';
import 'package:kafaat_loyalty/screens/profile/providers/ProfileProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ProfileScreen();
  }
}

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late LoadingButtonProvider buttonProvider;
  bool edit = false;
  bool _updating = false;
  Gender dropdownValue = Gender.Select;
  String? newBirthDate;

  late TextEditingController fullNameController;
  late TextEditingController emailController;
  late TextEditingController phoneController;
  late TextEditingController birthDateController;
  late TextEditingController genderController;

  @override
  void initState() {
    fullNameController = TextEditingController();
    emailController = TextEditingController();
    phoneController = TextEditingController();
    birthDateController = TextEditingController();
    genderController = TextEditingController();
    super.initState();
  }

  fetchProfile() async {
    Provider.of<ProfileProvider>(context, listen: false).fetchProfile(forceUpdate: true);
  }

  @override
  void dispose() {
    fullNameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    birthDateController.dispose();
    genderController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<ProfileProvider>(
        builder: (context, provider, child) {
          if (provider.response == null) {
            return SizedBox(
              height: MediaQuery.of(context).size.height - 100,
              child: const Center(child: CircularProgressIndicator(color: AppColors.brown)),
            );
          } else if (provider.response?.response is ApiException) {
            return SizedBox(
              height: MediaQuery.of(context).size.height - 100,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextWidget(text: provider.response?.response.businessError ?? 'Error occurred'),
                    SizedBox(height: 12.h),
                    InkWell(
                      child: TextWidget(text: 'Retry', color: AppColors.brown, size: 18.sp),
                      onTap: () => fetchProfile(),
                    ),
                  ],
                ),
              ),
            );
          } else {
            Profile profile = provider.response?.response;
            setData(provider.response?.response);
            return Column(
              children: [
                SizedBox(height: 24.h),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (profile.familyAccount != null && !profile.familyAccount!)
                      InkWell(
                        child: SvgPicture.asset(AppIcons.addUser, color: AppColors.brown),
                        onTap: () {
                          Provider.of<MainPageProvider>(context, listen: false).navigate(
                            MainPageItem(-1, AppStrings.familyMembers, AppIcons.image,
                                FamilyMembersPage(membersList: (provider.response?.response as Profile).familyAccounts ?? []), false),
                          );
                        },
                      )
                    else
                      SizedBox(width: 24.w),
                    Container(
                      height: 115.h,
                      width: 115.w,
                      // padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.w),
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        borderRadius: BorderRadius.circular(40),
                        boxShadow: const [
                          BoxShadow(color: AppColors.grey0D, blurRadius: 60),
                        ],
                      ),
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
                        child: CachedNetworkImage(
                          imageUrl: profile.company?.logo?.filePath ?? '',
                          errorWidget: (_, __, ___) => SvgPicture.asset(AppIcons.profileColored),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    InkWell(
                      child: Container(
                        height: 30.h,
                        width: 30.w,
                        padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 6.w),
                        decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: const [
                            BoxShadow(color: AppColors.grey0D, blurRadius: 6),
                          ],
                        ),
                        child: SvgPicture.asset(AppIcons.edit, color: AppColors.brown),
                      ),
                      onTap: () => setState(() => edit = !edit),
                    ),
                  ],
                ),
                SizedBox(height: 40.h),
                Form(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40.w),
                    child: Column(
                      children: [
                        textFormField(fullNameController, AppStrings.fullName, AppIcons.profile),
                        SizedBox(height: 30.h),
                        textFormField(emailController, AppStrings.emailAddress, AppIcons.send),
                        SizedBox(height: 30.h),
                        textFormField(phoneController, AppStrings.phoneNumber, AppIcons.call),
                        SizedBox(height: 30.h),
                        // textFormField(birthDateController, AppStrings.dateOfBirth, AppIcons.calendar),
                        InkWell(
                            onTap: !edit
                                ? null
                                : () async {
                                    DateTime? picked = await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(DateTime.now().year - 100),
                                      lastDate: DateTime.now(),
                                      builder: (context, child) => Theme(
                                        data: Theme.of(context).copyWith(
                                          colorScheme: const ColorScheme.light(
                                              primary: AppColors.brown, onPrimary: Colors.white, onSurface: Colors.black),
                                        ),
                                        child: child!,
                                      ),
                                    );

                                    if (picked != null) {
                                      birthDateController.text = picked.toString().substring(0, 10);
                                      newBirthDate = picked.toString().substring(0, 10);
                                    }
                                  },
                            child: TextFormField(
                              controller: birthDateController,
                              enabled: false,
                              decoration: InputDecoration(
                                label: TextWidget(text: AppStrings.dateOfBirth, color: AppColors.grey80),
                                prefixIcon: Align(
                                  alignment: AlignmentDirectional.centerStart,
                                  child: SvgPicture.asset(AppIcons.calendar, color: AppColors.grey80),
                                ),
                                prefixIconConstraints:
                                    BoxConstraints(minHeight: 18.h, maxHeight: 18.h, minWidth: 35.w, maxWidth: 35.w),
                                enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.black)),
                                focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.blue)),
                              ),
                            )),
                        SizedBox(height: 30.h),
                        DropdownButtonFormField<Gender>(
                          icon: SvgPicture.asset(AppIcons.chevronDown, color: AppColors.grey8F),
                          decoration: InputDecoration(
                            enabled: edit,
                            prefixIcon: Align(
                                alignment: AlignmentDirectional.centerStart,
                                child: SvgPicture.asset(AppIcons.user2, color: AppColors.grey80)),
                            prefixIconConstraints: BoxConstraints(minHeight: 18.h, maxHeight: 18.h, minWidth: 35.w, maxWidth: 35.w),
                            enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.greyCE)),
                            focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.greyCE)),
                            labelText: AppStrings.gender,
                            labelStyle: const TextStyle(color: AppColors.grey80),
                          ),
                          dropdownColor: AppColors.white,
                          value: profile.gender ?? dropdownValue,
                          onChanged: (Gender? newValue) => setState(() => dropdownValue = newValue!),
                          items: Gender.values.map<DropdownMenuItem<Gender>>((Gender value) {
                            return DropdownMenuItem<Gender>(value: value, child: TextWidget(text: value.name));
                          }).toList(),
                        ),
                        SizedBox(height: 46.h),
                        LoadingButton(
                          text: AppStrings.save,
                          radius: 8.r,
                          usedProvider: (provider) => buttonProvider = provider,
                          onTap: () {
                            if (edit) updateProfile();
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  TextFormField textFormField(TextEditingController controller, String label, String prefix) {
    return TextFormField(
      controller: controller,
      enabled: label != AppStrings.emailAddress ? edit : false,
      decoration: InputDecoration(
        label: TextWidget(text: label, color: AppColors.grey80),
        prefixIcon: Align(alignment: AlignmentDirectional.centerStart, child: SvgPicture.asset(prefix, color: AppColors.grey80)),
        prefixIconConstraints: BoxConstraints(minHeight: 18.h, maxHeight: 18.h, minWidth: 35.w, maxWidth: 35.w),
        enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.black)),
        focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: AppColors.blue)),
      ),
    );
  }

  Future setData(Profile profile) async {
    if (!_updating) {
      fullNameController = TextEditingController(text: '${profile.firstName} ${profile.lastName}');
      emailController = TextEditingController(text: profile.email);
      phoneController = TextEditingController(text: profile.phoneNumber ?? '');
      birthDateController = TextEditingController(text: newBirthDate ?? profile.birthDate ?? '');
      genderController = TextEditingController(text: profile.gender?.name);
      dropdownValue == profile.gender;
    }
  }

  updateProfile() async {
    _updating = true;
    buttonProvider.setLoading(true);
    final provider = Provider.of<ProfileProvider>(context, listen: false);
    Profile profile = provider.response?.response as Profile;
    final name = fullNameController.text.split(' ');

    await provider.updateProfile(Profile(
      id: profile.id,
      email: profile.email,
      firstName: name[0],
      lastName: name.sublist(1).join(' '),
      login: profile.login,
      phoneNumber: phoneController.text,
      gender: dropdownValue,
      birthDate: birthDateController.text,
    ));
    buttonProvider.setLoading(false);
    _updating = false;
  }
}
