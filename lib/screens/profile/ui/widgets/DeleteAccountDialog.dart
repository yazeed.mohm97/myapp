import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/onBoarding/ui/OnBoardingPage.dart';
import 'package:kafaat_loyalty/screens/profile/providers/ProfileProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/cahce/CacheManager.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/ButtonWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class DeleteAccountDialog extends StatelessWidget {
  const DeleteAccountDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Wrap(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Material(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(16.r),
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.all(16),
                child: Column(
                  children: [
                    const Icon(Icons.warning_amber_rounded, color: AppColors.red, size: 75),
                    SizedBox(height: 24.h),
                    TextWidget(text: AppStrings.deleteAccountWarning, size: 20.sp),
                    SizedBox(height: 24.h),
                    Row(
                      children: [
                        Expanded(
                          child: ButtonWidget(
                            onTap: () async {
                              final pp = Provider.of<ProfileProvider>(context, listen: false);
                              final mp = Provider.of<MainPageProvider>(context, listen: false);
                              var response = await pp.deleteAccount(pp.response!.response!.email);
                              if (response) {
                                pp.removeProfile();
                                mp.reset();
                                nService.pushAndRemove(const OnBoardingPage());
                              }
                            },
                            bgColor: AppColors.red,
                            text: AppStrings.yes,
                          ),
                        ),
                        SizedBox(width: 12.w),
                        Expanded(
                          child: ButtonWidget(
                            onTap: () => Navigator.pop(context),
                            bgColor: AppColors.green7F,
                            text: AppStrings.no,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
