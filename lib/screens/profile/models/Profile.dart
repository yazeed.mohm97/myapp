import 'dart:convert';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

import 'package:kafaat_loyalty/screens/profile/models/Gender.dart';

Profile profileFromJson(String str) => Profile.fromJson(json.decode(str));

String profileToJson(Profile data) => json.encode(data.toJson());

class Profile {
  Profile({
    this.id,
    this.login,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
    this.imageUrl,
    this.activated,
    this.langKey,
    this.createdBy,
    this.createdDate,
    this.lastModifiedBy,
    this.lastModifiedDate,
    this.authorities,
    this.address,
    this.enabled,
    this.gender,
    this.age,
    this.familyAccount,
    this.birthDate,
    this.company,
    this.familyAccounts,
  });

  int? id;
  String? login;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  String? imageUrl;
  bool? activated;
  String? langKey;
  String? createdBy;
  DateTime? createdDate;
  String? lastModifiedBy;
  DateTime? lastModifiedDate;
  List<String>? authorities;
  dynamic address;
  bool? enabled;
  Gender? gender;
  dynamic age;
  bool? familyAccount;
  dynamic birthDate;
  Company? company;
  List<String>? familyAccounts;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        login: json["login"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        email: json["email"],
        phoneNumber: json["phoneNumber"],
        imageUrl: json["imageUrl"],
        activated: json["activated"],
        langKey: json["langKey"],
        createdBy: json["createdBy"],
        createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
        lastModifiedBy: json["lastModifiedBy"],
        lastModifiedDate: json["lastModifiedDate"] == null ? null : DateTime.parse(json["lastModifiedDate"]),
        authorities: json["authorities"] == null ? [] : List<String>.from(json["authorities"]!.map((x) => x)),
        address: json["address"],
        enabled: json["enabled"],
        gender: json["gender"] == null
            ? Gender.Select
            : json["gender"] == 'MALE'
                ? Gender.Male
                : Gender.Female,
        age: json["age"],
        familyAccount: json["familyAccount"],
        birthDate: json["birthDate"],
        company: json["company"] == null ? null : Company.fromJson(json["company"]),
        familyAccounts: json["familyAccounts"] == null ? [] : List<String>.from(json["familyAccounts"]!.map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "login": login,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "phoneNumber": phoneNumber,
        "imageUrl": imageUrl,
        "activated": activated,
        "langKey": langKey,
        "createdBy": createdBy,
        "createdDate": createdDate?.toIso8601String(),
        "lastModifiedBy": lastModifiedBy,
        "lastModifiedDate": lastModifiedDate?.toIso8601String(),
        "authorities": authorities == null ? [] : List<dynamic>.from(authorities!.map((x) => x)),
        "address": address,
        "enabled": enabled,
        "gender": gender?.name.toUpperCase(),
        "age": age,
        "familyAccount": familyAccount,
        "birthDate": birthDate,
        "company": company?.toJson(),
        "familyAccounts": familyAccounts == null ? [] : List<dynamic>.from(familyAccounts!.map((x) => x)),
      };
}

class Company {
  Company({
    this.id,
    this.name,
    this.domain,
    this.logo,
    this.agreement,
    this.type,
    this.enabled,
  });

  int? id;
  String? name;
  String? domain;
  Logo? logo;
  dynamic agreement;
  Type? type;
  bool? enabled;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
        id: json["id"],
        name: json["name"],
        domain: json["domain"],
        logo: json["logo"] == null ? null : Logo.fromJson(json["logo"]),
        agreement: json["agreement"],
        type: json["type"] == null ? null : Type.fromJson(json["type"]),
        enabled: json["enabled"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "domain": domain,
        "logo": logo?.toJson(),
        "agreement": agreement,
        "type": type?.toJson(),
        "enabled": enabled,
      };
}

class Logo {
  Logo({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Logo.fromJson(Map<String, dynamic> json) => Logo(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}

class Type {
  Type({
    this.id,
    this.name,
  });

  int? id;
  String? name;

  factory Type.fromJson(Map<String, dynamic> json) => Type(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
