import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/profile/models/Profile.dart';
import 'package:kafaat_loyalty/screens/profile/repositories/ProfileRepository.dart';
import 'package:kafaat_loyalty/utils/cahce/CacheManager.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ProfileProvider extends ChangeNotifier {
  ///fetch profile
  static ApiResponseHandler? _response;

  ApiResponseHandler? get response => _response;

  fetchProfile({bool forceUpdate = false}) async {
    if (_response != null && _response?.response is ApiException) {
      _response = null;
      notifyListeners();
    } else if (_response == null || _response?.response is! Profile || forceUpdate) {
      _response = await ProfileRepository().getProfile();
      notifyListeners();
    }
  }

  /// logout then remove profile data from local cache.
  removeProfile() async {
    _response = null;
    UserCache.removeToken();
    notifyListeners();
  }

  deleteAccount(String mail) async {
    _response = await ProfileRepository().deleteAccount(mail);
    return _response?.statusCode == HttpStatus.ok;
    // notifyListeners();
  }

  ///update profile
  ApiResponseHandler? _updateResponse;

  ApiResponseHandler? get updateResponse => _updateResponse;

  updateProfile(Profile profile) async {
    print(profile.toJson());
    _updateResponse = await ProfileRepository().updateProfile(profile);
    await fetchProfile(forceUpdate: true);
    notifyListeners();
  }

  bool disposed = false;

  @override
  void notifyListeners() {
    if (!disposed) super.notifyListeners();
  }

  @override
  void dispose() {
    disposed = true;
    super.dispose();
  }
}
