import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:kafaat_loyalty/screens/dialogs/pdfViewer/PDFViewerScreen.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/models/GiftsCards.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/widgets/WidgetsMethods.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/ButtonWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class GiftCardDetailsPage extends StatelessWidget {
  const GiftCardDetailsPage({Key? key, required this.giftCard}) : super(key: key);
  final GiftCard giftCard;

  @override
  Widget build(BuildContext context) {
    return GiftCardDetailsScreen(giftCard: giftCard);
  }
}

class GiftCardDetailsScreen extends StatefulWidget {
  const GiftCardDetailsScreen({Key? key, required this.giftCard}) : super(key: key);
  final GiftCard giftCard;

  @override
  State<GiftCardDetailsScreen> createState() => _GiftCardDetailsScreenState();
}

class _GiftCardDetailsScreenState extends State<GiftCardDetailsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 24.h, width: double.infinity),
          Container(
            height: 366.h,
            width: 380.h,
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(6.r),
              boxShadow: const [
                BoxShadow(color: AppColors.grey0D, blurRadius: 16),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 14.h),
                Row(
                  children: [
                    SizedBox(width: 16.w),
                    TextWidget(text: AppStrings.expireOn, size: 12.sp, color: AppColors.grey8F),
                    TextWidget(
                        text: DateFormat('dd, MMMM yyyy').format(widget.giftCard.expirationDate ?? DateTime.now()),
                        size: 12.sp,
                        color: AppColors.brown),
                    const Spacer(),
                  ],
                ),
                Expanded(
                  child: Center(
                    child: widget.giftCard.image != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(6.r),
                            child: CachedNetworkImage(
                              imageUrl: widget.giftCard.image!.filePath!,
                              placeholder: (context, _) => SvgPicture.asset(AppIcons.image, height: 100.h, color: AppColors.grey8F),
                              errorWidget: (context, _, __) =>
                                  SvgPicture.asset(AppIcons.image, height: 100.h, color: AppColors.grey8F),
                              fit: BoxFit.fill,
                              width: double.infinity,
                            ),
                          )
                        : SvgPicture.asset(AppIcons.image, height: 100, color: AppColors.grey8F),
                  ),
                ),
                const Divider(height: 0),
              ],
            ),
          ),
          SizedBox(height: 30.h),
          if (widget.giftCard.addresses != null && widget.giftCard.addresses!.isNotEmpty)
            ButtonWidget(
              onTap: () {},
              bgColor: AppColors.white,
              textColor: AppColors.brown,
              borderColor: AppColors.brown,
              radius: 8.r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AppIcons.discovery, color: AppColors.brown),
                  SizedBox(width: 8.w),
                  TextWidget(text: AppStrings.viewOnMap, color: AppColors.brown),
                ],
              ),
            ),
          SizedBox(height: 25.h),
          if (false)
            ButtonWidget(
              onTap: () {},
              bgColor: AppColors.white,
              textColor: AppColors.brown,
              borderColor: AppColors.brown,
              radius: 8.r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AppIcons.discovery, color: AppColors.brown),
                  SizedBox(width: 8.w),
                  TextWidget(text: AppStrings.visitWebsite, color: AppColors.brown),
                ],
              ),
            ),
          if (widget.giftCard.attachment != null && widget.giftCard.attachment!.filePath!.isNotEmpty)
            ButtonWidget(
              margin: EdgeInsets.only(top: 8.h),
              onTap: () => wUtils.showCustomDialog(context, PDFViewerScreen(filePath: widget.giftCard.attachment!.filePath!)),
              bgColor: AppColors.white,
              textColor: AppColors.brown,
              borderColor: AppColors.brown,
              radius: 8.r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AppIcons.document, color: AppColors.brown),
                  SizedBox(width: 8.w),
                  TextWidget(text: AppStrings.viewDocument, color: AppColors.brown),
                ],
              ),
            ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 55.h, horizontal: 24.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextWidget(text: widget.giftCard.name ?? AppStrings.giftCard, color: AppColors.black, size: 18.sp),
                SizedBox(height: 9.h, width: double.infinity),
                TextWidget(text: widget.giftCard.details ?? '', color: AppColors.grey8F, size: 14.sp),
              ],
            ),
          )
        ],
      ),
    );
  }
}
