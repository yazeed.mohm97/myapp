import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/screens/offers/repositories/OffersRepository.dart';
import 'package:kafaat_loyalty/utils/LoadingState.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OffersProvider extends ChangeNotifier {
  ///get my offers
  ApiResponseHandler? _offers;

  ApiResponseHandler? get offers => _offers;

  ///full lists
  final List<Offers> _offersList = <Offers>[];
  final List<Offers> _discountsList = <Offers>[];

  List<Offers> get offersList => _offersList;

  List<Offers> get discountsList => _discountsList;

  ///home lists
  final List<Offer> _homeOffersList = <Offer>[];
  final List<Offer> _homeDiscountsList = <Offer>[];

  List<Offer> get homeOffersList => _homeOffersList;

  List<Offer> get homeDiscountsList => _homeDiscountsList;

  ///search lists
  final List<Offers> _searchOffersList = <Offers>[];
  final List<Offers> _searchDiscountsList = <Offers>[];

  List<Offers> get searchOffersList => _searchOffersList;

  List<Offers> get searchDiscountsList => _searchDiscountsList;

  getOffers({bool retry = false}) async {
    _offers ??= ApiResponseHandler(-1, LoadingState());
    if (retry) {
      _offers = null;
      notifyListeners();
    }

    _offers = await OffersRepository().getMyOffers();
    if (_offers?.statusCode == HttpStatus.ok && _offers?.response is List<Offers>) {
      ///? this hugeeeeeeee step to prevent pass by reference :)
      (_offers?.response as List<Offers>).map((main) {
        _offersList.add(Offers(category: main.category, categoryEn: main.categoryEn, offers: List.from(main.offers)));
        _discountsList.add(Offers(category: main.category, categoryEn: main.categoryEn, offers: List.from(main.offers)));
      }).toList();

      _offersList.map((e) => e.offers.removeWhere((element) => element.type != 'OFFER')).toList();
      _discountsList.map((e) => e.offers.removeWhere((element) => element.type != 'DISCOUNT')).toList();

      _offersList.map((e) => e.offers.map((e) => _homeOffersList.add(e)).toList()).toList();
      _discountsList.map((e) => e.offers.map((e) => _homeDiscountsList.add(e)).toList()).toList();
    }

    notifyListeners();
  }

  bookmark(int itemId, bool bookmarked) {
    for (var element in (_offers?.response as List<Offers>)) {
      element.offers.map((e) {
        if (e.id == itemId) e.bookedMarked = bookmarked;
      }).toList();
    }

    notifyListeners();
  }

  visited(int businessId) async {
    await OffersRepository().visited(businessId);
  }

  search(String query, {String? source}) {
    if (_offersList.isNotEmpty || _discountsList.isNotEmpty && (query.isNotEmpty)) {
      if (source == 'HOME') {
        _homeOffersList.clear();
        _homeDiscountsList.clear();
      } else if (source == 'OFFER') {
        _searchOffersList.clear();
      } else if (source == 'DISCOUNT') {
        _searchDiscountsList.clear();
      }
      _offersList.map((e) {
        return e.offers.map((offer) {
          if (offer.title!.contains(query) || offer.description!.contains(query)) {
            if (source == 'HOME') {
              _homeOffersList.add(offer);
            } else if (source == 'OFFER') {
              List<Offer> internalOffers = [];
              if (offer.title!.contains(query) || offer.description!.contains(query)) {
                internalOffers.add(offer);
              }
              Offers newOffers = Offers(category: e.category, categoryEn: e.categoryEn, offers: internalOffers);
              _searchOffersList.add(newOffers);
            }
          }
        }).toList();
      }).toList();

      _discountsList.map((e) {
        e.offers.map((offer) {
          if (offer.title!.contains(query) || offer.description!.contains(query)) {
            if (source == 'HOME') {
              _homeDiscountsList.add(offer);
            } else if (source == 'DISCOUNT') {
              List<Offer> internalOffers = [];
              if (offer.title!.contains(query) || offer.description!.contains(query)) {
                internalOffers.add(offer);
              }
              Offers newOffers = Offers(category: e.category, categoryEn: e.categoryEn, offers: internalOffers);
              _searchDiscountsList.add(newOffers);
            }
          }
        }).toList();
      }).toList();
    } else {
      _searchOffersList.clear();
      if (_offers?.statusCode == HttpStatus.ok && _offers?.response is List<Offers>) {
        ///? this hugeeeeeeee step to prevent pass by reference :)
        (_offers?.response as List<Offers>).map((main) {
          _offersList.add(Offers(category: main.category, categoryEn: main.categoryEn, offers: List.from(main.offers)));
          _discountsList.add(Offers(category: main.category, categoryEn: main.categoryEn, offers: List.from(main.offers)));
        }).toList();

        _offersList.map((e) => e.offers.removeWhere((element) => element.type != 'OFFER')).toList();
        _discountsList.map((e) => e.offers.removeWhere((element) => element.type != 'DISCOUNT')).toList();

        _offersList.map((e) => e.offers.map((e) => _homeOffersList.add(e)).toList()).toList();
        _discountsList.map((e) => e.offers.map((e) => _homeDiscountsList.add(e)).toList()).toList();
      }
    }

    notifyListeners();
  }
}
