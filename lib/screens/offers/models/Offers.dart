import 'dart:convert';
import 'package:easy_localization/easy_localization.dart';
import 'package:kafaat_loyalty/screens/map/models/Location.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

List<Offers> offersFromJson(String str) => List<Offers>.from(json.decode(str).map((x) => Offers.fromJson(x)));

String offersToJson(List<Offers> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Offers {
  Offers({
    required this.category,
    required this.categoryEn,
    this.categoryName,
    required this.offers,
  }) {
    categoryName = NavigationService.navigatorKey.currentContext!.locale.languageCode.toLowerCase() == 'ar' ? category : categoryEn;
  }

  String category;
  String categoryEn;
  String? categoryName;
  List<Offer> offers;

  factory Offers.fromJson(Map<String, dynamic> json) => Offers(
        category: json["category"],
        categoryEn: json["categoryEn"],
        offers: json["offers"] == null ? [] : List<Offer>.from(json["offers"]!.map((x) => Offer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "category": category,
        "categoryEn": categoryEn,
        "offers": offers == null ? [] : List<dynamic>.from(offers.map((x) => x.toJson())),
      };
}

class Offer {
  Offer({
    this.id,
    this.title,
    this.titleEn,
    this.type,
    this.description,
    this.descriptionEn,
    this.expirationDate,
    this.enabled,
    this.bookedMarked,
    this.bookedMarkId,
    this.qr,
    this.code,
    this.percentage,
    this.agreement,
    this.image,
    this.details,
    this.contact,
    this.businessName,
    this.businessId,
    this.locations,
  }) {
    titleText =
        NavigationService.navigatorKey.currentContext!.locale.languageCode.toLowerCase() == 'ar' ? title ?? titleEn : titleEn ?? title;
    descriptionText = NavigationService.navigatorKey.currentContext!.locale.languageCode.toLowerCase() == 'ar'
        ? description ?? descriptionEn
        : descriptionEn ?? description;
  }

  String? titleText;
  String? descriptionText;

  int? id;
  String? title;
  String? titleEn;
  String? type;
  String? description;
  String? descriptionEn;
  DateTime? expirationDate;
  bool? enabled;
  bool? bookedMarked;
  int? bookedMarkId;
  String? qr;
  String? code;
  double? percentage;
  Agreement? agreement;
  Agreement? image;
  Details? details;
  dynamic contact;
  String? businessName;
  int? businessId;
  List<Location>? locations;

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
        id: json["id"],
        title: json["title"],
        titleEn: json["titleEn"],
        type: json["type"],
        description: json["description"],
        descriptionEn: json["descriptionEn"],
        expirationDate: json["expirationDate"] == null ? null : DateTime.parse(json["expirationDate"]),
        enabled: json["enabled"],
        bookedMarked: json["bookedMarked"],
        bookedMarkId: json["bookedMarkId"],
        qr: json["qr"],
        code: json["code"],
        percentage: json["percentage"],
        agreement: json["agreement"] == null ? null : Agreement.fromJson(json["agreement"]),
        image: json["image"] == null ? null : Agreement.fromJson(json["image"]),
        details: json["details"] == null ? null : Details.fromJson(json["details"]),
        contact: json["contact"],
        businessName: json["businessName"],
        businessId: json["businessId"],
        locations: json["locations"] == null ? [] : List<Location>.from(json["locations"]!.map((x) => Location.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "titleEn": titleEn,
        "type": type,
        "description": description,
        "descriptionEn": descriptionEn,
        "expirationDate":
            "${expirationDate!.year.toString().padLeft(4, '0')}-${expirationDate!.month.toString().padLeft(2, '0')}-${expirationDate!.day.toString().padLeft(2, '0')}",
        "enabled": enabled,
        "bookedMarked": bookedMarked,
        "bookedMarkId": bookedMarkId,
        "qr": qr,
        "code": code,
        "percentage": percentage,
        "agreement": agreement?.toJson(),
        "image": image?.toJson(),
        "details": details,
        "contact": contact,
        "businessName": businessName,
        "businessId": businessId,
        "locations": locations == null ? [] : List<dynamic>.from(locations!.map((x) => x.toJson())),
      };
}

class Agreement {
  Agreement({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Agreement.fromJson(Map<String, dynamic> json) => Agreement(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}

class Details {
  Details({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}
