import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestOffers/LatestOfferItem.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/searchWidget/SearchTextField.dart';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/screens/offers/providers/OffersProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/widgets/exceptions/RetryWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OffersPage extends StatelessWidget {
  const OffersPage({Key? key, required this.type}) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    return MyOffersScreen(type: type);
  }
}

class MyOffersScreen extends StatefulWidget {
  const MyOffersScreen({Key? key, required this.type}) : super(key: key);
  final String type;

  @override
  State<MyOffersScreen> createState() => _MyOffersScreenState();
}

class _MyOffersScreenState extends State<MyOffersScreen> with TickerProviderStateMixin {
  late ScrollController _scrollController;
  late AnimationController _animationController;
  TextEditingController searchController = TextEditingController();
  int selectedPosition = 0;

  @override
  void initState() {
    _scrollController = ScrollController();
    _animationController = AnimationController(
      value: 0,
      duration: const Duration(milliseconds: 250),
      vsync: this,
      reverseDuration: const Duration(milliseconds: 250),
    );

    super.initState();
  }

  fetchOffers({bool retry = false}) {
    Provider.of<OffersProvider>(context, listen: false).getOffers(retry: retry);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 21.w),
      child: Consumer<OffersProvider>(
        builder: (context, provider, _) {
          if (provider == null) {
            return SizedBox(
              height: MediaQuery.of(context).size.height / 1.5,
              child: const Center(child: CircularProgressIndicator(color: AppColors.brown)),
            );
          } else if (provider.offers?.response is ApiException) {
            return RetryWidget(
              message: provider.offers?.response.businessError,
              action: () => fetchOffers(retry: true),
            );
          } else {
            List<Offers> items = widget.type == 'OFFER'
                ? provider.searchOffersList.isNotEmpty
                    ? Provider.of<OffersProvider>(context, listen: false).searchOffersList
                    : Provider.of<OffersProvider>(context, listen: false).offersList
                : provider.searchDiscountsList.isNotEmpty
                    ? Provider.of<OffersProvider>(context, listen: false).searchDiscountsList
                    : Provider.of<OffersProvider>(context, listen: false).discountsList;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16.h),
                SearchTextField(searchController: searchController, source: widget.type),
                SizedBox(height: 29.h),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      items.length,
                      (index) => Padding(
                        padding: items[index].offers.isNotEmpty ? EdgeInsetsDirectional.only(start: 8.w, end: 16.w) : EdgeInsets.zero,
                        child: items[index].offers.isNotEmpty
                            ? InkWell(
                                child: Column(
                                  children: [
                                    TextWidget(text: items[index].categoryName ?? '', color: AppColors.grey8F),
                                    Container(
                                      height: 5.h,
                                      width: 25.w,
                                      margin: EdgeInsets.only(top: 8.h),
                                      decoration: BoxDecoration(
                                        color: selectedPosition == index ? AppColors.brown : Colors.transparent,
                                        borderRadius: BorderRadius.circular(12.r),
                                      ),
                                    ),
                                  ],
                                ),
                                onTap: () {
                                  _scrollController.animateTo(
                                    200.0 * index,
                                    duration: const Duration(milliseconds: 250),
                                    curve: Curves.easeIn,
                                  );
                                  setState(() => selectedPosition = index);
                                  _animationController.animateTo(0.5);
                                  Timer(const Duration(milliseconds: 750), () => _animationController.animateTo(0));
                                },
                              )
                            : const SizedBox.shrink(),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 52.h),
                AnimatedBuilder(
                    animation: _animationController,
                    builder: (_, child) {
                      return Expanded(
                        child: ListView.builder(
                          controller: _scrollController,
                          itemCount: items.length,
                          itemBuilder: (context, index) {
                            return items[index].offers.isNotEmpty
                                ? Stack(
                                    children: [
                                      Visibility(
                                        visible: selectedPosition == index,
                                        child: Opacity(
                                          opacity: _animationController.value,
                                          child: Container(
                                            height: 100.h,
                                            width: double.infinity,
                                            decoration: const BoxDecoration(
                                              gradient: LinearGradient(
                                                colors: [
                                                  AppColors.white,
                                                  AppColors.brown,
                                                  AppColors.white,
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          TextWidget(
                                              text: items[index].categoryName ?? '',
                                              color: AppColors.black,
                                              size: 16.sp,
                                              fontWeight: AppFonts.medium),
                                          SizedBox(height: 9.h, width: double.infinity),
                                          TextWidget(
                                            text: AppStrings.checkTheLatestAddedOffers,
                                            color: AppColors.grey8F,
                                            size: 14.sp,
                                            fontWeight: AppFonts.regular,
                                          ),
                                          SizedBox(height: 22.h),
                                          SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: Row(
                                              children: List.generate(
                                                items[index].offers.length,
                                                (position) {
                                                  return LatestOfferItem(item: items[index].offers[position], backScreen: widget);
                                                },
                                              ),
                                            ),
                                          ),
                                          SizedBox(height: 45.h),
                                        ],
                                      ),
                                    ],
                                  )
                                : const SizedBox.shrink();
                          },
                        ),
                      );
                    })
              ],
            );
          }
        },
      ),
    );
  }
}
