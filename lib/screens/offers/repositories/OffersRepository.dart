import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OffersRepository {
  Future<ApiResponseHandler?> getMyOffers() async {
    var response = await HttpClient().get(endPoint: 'offers?sort=discount,asc');
    if (response.statusCode == 200) {
      if (response.response is! List<Offers>) {
        response.response = offersFromJson(response.response);
      }
    }
    return response;
  }

  Future visited(int businessId) async {
    await HttpClient().get(endPoint: 'businesses/$businessId/visited');
  }
}
