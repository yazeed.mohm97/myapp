import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ChangePasswordRepository {
  Future<bool> changePassword(Map<String, String> password) async {
    ApiResponseHandler response = await HttpClient().post(endPoint: 'account/reset-password/init', payload: password);
    return response.statusCode == 200;
  }
}

ChangePasswordRepository cpRepository = ChangePasswordRepository();
