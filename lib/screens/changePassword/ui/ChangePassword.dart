import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/changePassword/repositories/ChangePasswordRepository.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangePasswordScreen();
  }
}

class ChangePasswordScreen extends StatelessWidget {
  ChangePasswordScreen({Key? key}) : super(key: key);
  late LoadingButtonProvider _buttonProvider;
  final _formKey = GlobalKey<FormState>();

  TextEditingController currentPassword = TextEditingController();
  TextEditingController newPassword = TextEditingController();
  TextEditingController repeatPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/images/login.svg', height: 214.h, width: 232.89.w),
            SizedBox(height: 32.81.h),
            TextWidget(text: AppStrings.changePassword, color: AppColors.brown, size: 26.sp),
            SizedBox(height: 10.h),
            TextWidget(text: AppStrings.toChangePassword, color: AppColors.grey8F, size: 14.sp),
            SizedBox(height: 60.h),
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 40.w),
                child: Column(
                  children: [
                    TextFormField(
                      controller: currentPassword,
                      validator: (value) {
                        if (value == null || value.isEmpty) AppStrings.pleaseEnterYourPassword;
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: AppStrings.currentPassword,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        suffixIcon: SvgPicture.asset(AppIcons.show, color: AppColors.greyBF, height: 15.h, width: 19.w),
                        prefixIconConstraints: BoxConstraints(maxHeight: 17.h, maxWidth: 20.w, minHeight: 17.h, minWidth: 20.w),
                        suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                      ),
                    ),
                    SizedBox(height: 30.h),
                    TextFormField(
                      controller: newPassword,
                      validator: (value) {
                        if (value == null || value.isEmpty) AppStrings.pleaseEnterYourPassword;
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: AppStrings.newPassword,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        suffixIcon: SvgPicture.asset(AppIcons.show, color: AppColors.greyBF, height: 15.h, width: 19.w),
                        prefixIconConstraints: BoxConstraints(maxHeight: 17.h, maxWidth: 20.w, minHeight: 17.h, minWidth: 20.w),
                        suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                      ),
                    ),
                    SizedBox(height: 30.h),
                    TextFormField(
                      controller: repeatPassword,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return AppStrings.pleaseEnterYourPassword;
                        } else if (value != newPassword.text) {
                          return AppStrings.passwordDoesNotMatch;
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: AppStrings.repeatPassword,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        suffixIcon: SvgPicture.asset(AppIcons.show, color: AppColors.greyBF, height: 15.h, width: 19.w),
                        prefixIconConstraints: BoxConstraints(maxHeight: 17.h, maxWidth: 20.w, minHeight: 17.h, minWidth: 20.w),
                        suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                      ),
                    ),
                    SizedBox(height: 40.h),
                    Align(
                      alignment: AlignmentDirectional.centerEnd,
                      child: TextWidget(text: AppStrings.forgetPassword, color: AppColors.brown),
                    ),
                    LoadingButton(
                      text: AppStrings.save,
                      radius: 8.r,
                      margin: EdgeInsets.only(top: 75.h, bottom: 29.h),
                      usedProvider: (provider) => _buttonProvider = provider,
                      onTap: () async {
                        _buttonProvider.setLoading(true);
                        var response = await cpRepository.changePassword(
                          {'currentPassword': currentPassword.text, 'newPassword': newPassword.text},
                        );
                        _buttonProvider.setLoading(false);
                      },
                    ),
                    // SizedBox(height: 40.h),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
