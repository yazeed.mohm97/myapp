import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

List<Location> locationsFromJson(String str) => List<Location>.from(json.decode(str).map((x) => Location.fromJson(x)));

String locationToJson(List<Location> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Location {
  Location({
    this.id,
    this.latitude,
    this.longitude,
    this.name,
    this.nameEn,
    this.generateQr,
    this.qrCode,
    this.businessName,
    this.businessId,
    this.businessLogo,
    this.marker,
  });

  int? id;
  double? latitude;
  double? longitude;
  String? name;
  String? nameEn;
  bool? generateQr;
  String? qrCode;
  String? businessName;
  int? businessId;
  BusinessLogo? businessLogo;
  BitmapDescriptor? marker;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        latitude: double.parse(json["latitude"]),
        longitude: double.parse(json["longitude"]),
        name: json["name"],
        nameEn: json["nameEn"],
        generateQr: json["generateQR"],
        qrCode: json["qrCode"],
        businessName: json["businessName"],
        businessId: json["businessId"],
        businessLogo: json["businessLogo"] == null ? null : BusinessLogo.fromJson(json["businessLogo"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "name": name,
        "nameEn": nameEn,
        "generateQR": generateQr,
        "qrCode": qrCode,
        "businessName": businessName,
        "businessId": businessId,
        "businessLogo": businessLogo?.toJson(),
      };
}

class BusinessLogo {
  BusinessLogo({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory BusinessLogo.fromJson(Map<String, dynamic> json) => BusinessLogo(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}
