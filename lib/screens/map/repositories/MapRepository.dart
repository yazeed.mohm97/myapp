import 'package:kafaat_loyalty/screens/map/models/Location.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MapRepository {
  Future<ApiResponseHandler> getAllLocations() async {
    var response = await HttpClient().get(endPoint: 'locations');
    if (response.statusCode == 200) {
      response.response = locationsFromJson(response.response);
      print('length1: ${(response.response as List).length}');
    }
    return response;
  }
}

MapRepository mapRepository = MapRepository();
