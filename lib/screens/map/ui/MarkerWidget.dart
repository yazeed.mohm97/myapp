import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/map/models/Location.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MarkerWidget extends StatelessWidget {
  const MarkerWidget({super.key, required this.item});

  final Location item;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80.w,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 60.h,
            width: 60.w,
            padding: const EdgeInsets.all(6),
            decoration: BoxDecoration(
                color: AppColors.white,
                shape: BoxShape.circle,
                border: Border.all(color: AppColors.brown, width: 6.w),
                image: DecorationImage(image: CachedNetworkImageProvider(item.businessLogo!.filePath!), fit: BoxFit.contain)),
          ),
          Container(height: 19.h, width: 6.w, color: AppColors.brown),
        ],
      ),
    );
  }
}
