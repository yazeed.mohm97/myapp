import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/searchWidget/SearchTextField.dart';
import 'package:kafaat_loyalty/screens/map/controllers/MapController.dart';
import 'package:kafaat_loyalty/screens/map/models/Location.dart';
import 'package:kafaat_loyalty/screens/map/repositories/MapRepository.dart';
import 'package:kafaat_loyalty/screens/map/ui/MapInformationCard.dart';
import 'package:kafaat_loyalty/screens/map/ui/MarkerWidget.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:widget_to_marker/widget_to_marker.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MapPage extends StatelessWidget {
  const MapPage({Key? key, this.locations}) : super(key: key);
  final List<Location>? locations;

  @override
  Widget build(BuildContext context) {
    return MapScreen(locations: locations);
  }
}

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key, this.locations}) : super(key: key);
  final List<Location>? locations;

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  TextEditingController searchController = TextEditingController();
  final Completer<GoogleMapController> _completer = Completer();
  late GoogleMapController _controller;
  final Set<Marker> _markers = <Marker>{};

  final CameraPosition _riyadh = const CameraPosition(target: LatLng(24.7133028, 46.761493), zoom: 14.4746);
  LatLng? _myLocation;

  bool _isLoading = true;
  final List<Location> _locations = <Location>[];
  final List<Location> _searchLocations = <Location>[];

  Location? clickedLocation;

  @override
  void initState() {
    getLocations();
    searchController.addListener(() => _search());
    super.initState();
  }

  _getMyLocation() async {
    _myLocation = await MapController().getMyLocation();
    _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: _myLocation!, zoom: 12)));
  }

  getLocations() async {
    if (widget.locations == null) {
      // var response = await MapRepository().getAllLocations();
      // if (response.statusCode == 200 && response.response is List<Location>) {
      //         _locations.addAll(response.response);
      //         _searchLocations.addAll(response.response);
      //         _addMarkers();
      //       }

      List<Location> response = await mapController.getLocations();
      if (response.isNotEmpty) {
        _locations.addAll(response);
        _searchLocations.addAll(response);
        _addMarkers();
      }
    } else {
      _locations.addAll(widget.locations!);
      _searchLocations.addAll(widget.locations!);
      _addMarkers();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _riyadh,
          myLocationEnabled: true,
          myLocationButtonEnabled: false,
          zoomControlsEnabled: false,
          markers: _markers,
          onMapCreated: (GoogleMapController controller) async {
            _controller = controller;
            _completer.complete(controller);

            String value = await DefaultAssetBundle.of(context).loadString('assets/styles/mapStyle.json');
            controller.setMapStyle(value);

            await _getMyLocation();
          },
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: EdgeInsets.only(top: 16.h),
            child: SearchTextField(searchController: searchController, hint: AppStrings.searchForStoresOrBranches),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            const SizedBox(width: double.infinity),
            if (_myLocation != null)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 32.h, horizontal: 16.w),
                child: FloatingActionButton(
                  backgroundColor: AppColors.white,
                  child: const Icon(Icons.my_location, color: AppColors.blackLight),
                  onPressed: () {
                    _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: _myLocation!, zoom: 12)));
                  },
                ),
              ),
            if (clickedLocation != null)
              Align(
                alignment: Alignment.bottomCenter,
                child: MapInformationCard(location: clickedLocation!),
              ),
          ],
        ),
        if (_isLoading)
          Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.grey.withOpacity(0.5),
            child: const Center(
              child: CircularProgressIndicator(),
            ),
          ),
      ],
    );
  }

  void _addMarkers() async {
    _markers.clear();
    for (var i = 0; i < _searchLocations.length; ++i) {
      var o = _searchLocations[i];
      if (o.latitude != null && o.longitude != null) {
        _markers.add(
          Marker(
            markerId: MarkerId(o.hashCode.toString()),
            position: LatLng(o.latitude!, o.longitude!),
            onTap: () => setState(() => clickedLocation = o),
            icon: await MarkerWidget(item: o).toBitmapDescriptor(waitToRender: const Duration(milliseconds: 100)),
          ),
        );
      }
    }
    setState(() => _isLoading = false);
    // setState(() {});
  }

  _search() {
    if (searchController.text.isNotEmpty) {
      _searchLocations.clear();
      _locations.map((e) {
        if ((e.name != null && e.name!.contains(searchController.text)) || e.businessName != null && e.businessName!.contains(searchController.text)) {
          _searchLocations.add(e);
          _addMarkers();
        }
      }).toList();
    } else {
      _searchLocations.addAll(_locations);
      _addMarkers();
    }
  }
}
