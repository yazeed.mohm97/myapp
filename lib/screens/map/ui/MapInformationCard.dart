import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kafaat_loyalty/screens/business/ui/BusinessPage.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/map/models/Location.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MapInformationCard extends StatelessWidget {
  const MapInformationCard({Key? key, required this.location}) : super(key: key);
  final Location location;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      child: InkWell(
        child: Card(
          child: Container(
            height: 150.h,
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 16.w),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.r),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CachedNetworkImage(
                  imageUrl: location.businessLogo?.filePath ?? '',
                  errorWidget: (context, _, __) => SvgPicture.asset(AppIcons.image, height: 100.h),
                  width: MediaQuery.of(context).size.width * 0.2,
                ),
                SizedBox(width: 24.w),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextWidget(text: location.businessName ?? '(No Title)', size: 18.sp),
                      SizedBox(height: 12.h),
                      TextWidget(text: location.name ?? '', color: AppColors.grey8F, size: 14.sp),
                    ],
                  ),
                ),
                InkWell(
                  child: const Icon(Icons.directions, color: AppColors.blue, size: 40),
                  onTap: () => _launchMaps(LatLng(location.latitude!, location.longitude!)),
                ),
              ],
            ),
          ),
        ),
        onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
          MainPageItem(-1, location.businessName ?? '', AppIcons.image, BusinessPage(businessId: location.businessId!), false),
        ),
      ),
    );
  }

  _launchMaps(LatLng latlng) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=${latlng.latitude},${latlng.longitude}';
    String appleUrl = 'https://maps.apple.com/?sll=${latlng.latitude},${latlng.longitude}';
    if (Platform.isAndroid && await canLaunchUrl(Uri.parse(googleUrl))) {
      await launchUrl(Uri.parse(googleUrl));
    } else if (Platform.isIOS && await canLaunchUrl(Uri.parse(googleUrl))) {
      await launchUrl(Uri.parse(googleUrl));
    } else if (Platform.isIOS && await canLaunchUrl(Uri.parse(appleUrl))) {
      await launchUrl(Uri.parse(appleUrl));
    } else {
      throw 'Could not launch url';
    }
  }
}
