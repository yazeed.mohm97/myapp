import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BoundaryMarker extends StatefulWidget {
  const BoundaryMarker({Key? key}) : super(key: key);

  @override
  State<BoundaryMarker> createState() => _BoundaryMarkerState();
}

class _BoundaryMarkerState extends State<BoundaryMarker> {
  final GlobalKey _globalKey = GlobalKey();

  Future<Uint8List?> _capturePng() async {
    RenderRepaintBoundary? boundary = _globalKey.currentContext?.findRenderObject() as RenderRepaintBoundary;
    ui.Image image = await boundary.toImage(pixelRatio: 3.0);
    ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    var pngBytes = byteData!.buffer.asUint8List();
    var bs64 = base64Encode(pngBytes);

    return pngBytes;
  }

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: _globalKey,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Widget To Image demo'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                'click below given button to capture iamge',
              ),
              InkWell(
                onTap: _capturePng,
                child: const Text('capture Image'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
