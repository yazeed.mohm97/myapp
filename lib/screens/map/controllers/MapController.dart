import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kafaat_loyalty/screens/map/models/Location.dart' as l;
import 'package:kafaat_loyalty/screens/map/repositories/MapRepository.dart';
import 'package:kafaat_loyalty/screens/map/ui/MarkerWidget.dart';
import 'package:location/location.dart';
import 'package:widget_to_marker/widget_to_marker.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class MapController {
  static List<l.Location> locations = <l.Location>[];
  Map<String, BitmapDescriptor?> markers = {};

  Future<List<l.Location>> getLocations() async {
    if (locations.isEmpty) {
      var response = await mapRepository.getAllLocations();
      if (response.statusCode == 200 && response.response is List<l.Location>) {
        locations = response.response;

        for (var location in locations) {
          if (markers.containsKey(location.businessLogo!.filePath!)) {
            location.marker = markers[location.businessLogo!.filePath!];
          } else {
            BitmapDescriptor icon = await MarkerWidget(item: location).toBitmapDescriptor(waitToRender: const Duration(milliseconds: 100));
            markers[location.businessLogo!.filePath!] = icon;
            location.marker = location.marker;
          }
        }
      }
    }
    return locations;
  }

  Future<LatLng?> getMyLocation() async {
    Location location = Location();
    bool serviceEnabled = await location.serviceEnabled();
    PermissionStatus permissionGranted = await location.hasPermission();

    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return null;
      }
    }
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return null;
      }
    }

    LocationData locationData = await location.getLocation();

    return LatLng(locationData.latitude!, locationData.longitude!);
  }
}

MapController mapController = MapController();
