import 'dart:io';

import 'package:kafaat_loyalty/screens/bookmarks/models/Bookmark.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BookmarksRepository {
  Future<ApiResponseHandler> getBookmarks() async {
    var response = await HttpClient().get(endPoint: 'bookmarks');
    if (response.statusCode == HttpStatus.ok) {
      response.response = bookmarksFromJson(response.response);
    }
    return response;
  }

  Future<ApiResponseHandler> addBookmark(int itemId) async {
    var response = await HttpClient().post(endPoint: 'bookmarks?offerId=$itemId');
    return response;
  }

  Future<ApiResponseHandler> deleteBookmark(int itemId) async {
    var response = await HttpClient().delete(endPoint: 'bookmarks/$itemId');
    return response;
  }

  Future<ApiResponseHandler> deleteBookmarkByOfferId(int offerId) async {
    var response = await HttpClient().delete(endPoint: 'bookmarks/offer/$offerId');
    return response;
  }
}
