import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/bookmarks/models/Bookmark.dart';
import 'package:kafaat_loyalty/screens/bookmarks/repositories/BookmarksRepository.dart';
import 'package:kafaat_loyalty/utils/LoadingState.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BookmarksProvider extends ChangeNotifier {
  ///get my bookmarks
  static ApiResponseHandler? _bookmarks;

  ApiResponseHandler? get bookmarks => _bookmarks;

  final List<Bookmark> _offersBookmarks = [];
  final List<Bookmark> _discountsBookmarks = [];

  List<Bookmark> get offersBookmarks => _offersBookmarks;

  List<Bookmark> get discountsBookmarks => _discountsBookmarks;

  Future getBookmarks() async {
    _bookmarks = ApiResponseHandler(-1, LoadingState());
    _offersBookmarks.clear();
    _discountsBookmarks.clear();
    _bookmarks = await BookmarksRepository().getBookmarks();
    if (_bookmarks?.statusCode == 200 && _bookmarks?.response is List<Bookmark>) {
      (_bookmarks?.response as List<Bookmark>).map((element) {
        if (element.offer?.type == 'OFFER') {
          _offersBookmarks.add(element);
        } else if (element.offer?.type == 'DISCOUNT') {
          _discountsBookmarks.add(element);
        }
      }).toList();
    }
    notifyListeners();
  }

  /// add bookmark.
  ApiResponseHandler? _addResponse;

  ApiResponseHandler? get addResponse => _addResponse;

  Future<ApiResponseHandler?> addBookMark(int itemId) async {
    return await BookmarksRepository().addBookmark(itemId);
  }

  Future<ApiResponseHandler?> deleteBookMark(int itemId) async {
    return await BookmarksRepository().deleteBookmark(itemId);
  }

  Future<ApiResponseHandler?> deleteBookmarkByOfferId(int itemId) async {
    return await BookmarksRepository().deleteBookmarkByOfferId(itemId);
  }
}
