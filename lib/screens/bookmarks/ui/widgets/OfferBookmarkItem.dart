import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/bookmarks/models/Bookmark.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class OfferBookmarkItem extends StatelessWidget {
  const OfferBookmarkItem({Key? key, required this.item, required this.backScreen}) : super(key: key);
  final Bookmark item;
  final Widget backScreen;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: 115.h,
        width: 382.w,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 4.w),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(6.r),
          boxShadow: const [
            BoxShadow(color: AppColors.grey0D, blurRadius: 10),
          ],
        ),
        child: Row(
          children: [
            SizedBox(
              height: double.infinity,
              width: ScreenUtil.defaultSize.width / 3,
              child: item.offer?.image?.filePath != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(6.r),
                      child: CachedNetworkImage(
                        imageUrl: item.offer!.image!.filePath!,
                        placeholder: (context, _) => SvgPicture.asset(AppIcons.image, color: AppColors.black, height: 75.h),
                        fit: BoxFit.fill,
                      ),
                    )
                  : Center(
                      child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 21.w),
                      child: SvgPicture.asset(AppIcons.image, color: AppColors.black, height: 75.h),
                    )),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: AlignmentDirectional.topEnd,
                    child: InkWell(
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(top: 10.h, end: 10.w),
                        child: SvgPicture.asset(AppIcons.filledHeart),
                      ),
                      onTap: () async {},
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(start: 31.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20.h),
                          TextWidget(
                              text: item.offer?.title ?? '(No title)',
                              color: AppColors.black,
                              size: 12.sp,
                              fontWeight: AppFonts.medium),
                          SizedBox(height: 10.h),
                          TextWidget(
                            text: item.offer?.description ?? '',
                            color: AppColors.black,
                            size: 10.sp,
                            fontWeight: AppFonts.regular,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        // Provider.of<MainPageProvider>(context, listen: false).setScreen(
        //   newScreen: OfferPage(offer: item),
        //   title: item.title ?? '',
        //   backScreen: backScreen,
        // );
      },
    );
  }
}
