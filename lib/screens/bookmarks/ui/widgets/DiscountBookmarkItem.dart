import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/bookmarks/models/Bookmark.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class DiscountBookmarkItem extends StatelessWidget {
  const DiscountBookmarkItem({Key? key, required this.items}) : super(key: key);
  final List<Bookmark> items;

  @override
  Widget build(BuildContext context) {
    return items.isEmpty
        ? SizedBox(
            height: 200.h,
            child: Center(child: TextWidget(text: AppStrings.noItems)),
          )
        : Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 22.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWidget(text: AppStrings.latestDiscounts, color: AppColors.black, size: 16.sp, fontWeight: AppFonts.medium),
                        SizedBox(height: 9.h),
                        TextWidget(
                          text: AppStrings.checkTheLatestAddedDiscounts,
                          color: AppColors.grey8F,
                          size: 14.sp,
                          fontWeight: AppFonts.regular,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 21.h),
              GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (context, position) {
                  Bookmark item = items[position];
                  return InkWell(
                    child: Container(
                      height: 180.h,
                      width: 180.w,
                      margin: const EdgeInsets.all(4),
                      decoration: const BoxDecoration(
                        color: AppColors.white,
                        boxShadow: [BoxShadow(color: AppColors.grey0D, blurRadius: 24)],
                      ),
                      child: Stack(
                        children: [
                          Stack(
                            children: [
                              item.offer?.image?.filePath != null
                                  ? ClipRRect(
                                      borderRadius: BorderRadius.circular(6.r),
                                      child: CachedNetworkImage(
                                        imageUrl: item.offer?.image?.filePath ?? '',
                                        placeholder: (context, _) => SvgPicture.asset(
                                          AppIcons.image,
                                          color: AppColors.black,
                                          height: 75.h,
                                        ),
                                        height: double.infinity,
                                        fit: BoxFit.fill,
                                      ),
                                    )
                                  : Center(child: SvgPicture.asset(AppIcons.image, color: AppColors.black, height: 75.h)),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: TextWidget(text: item.offer?.title ?? '(No title)'),
                              )
                            ],
                          ),
                          Align(
                            alignment: AlignmentDirectional.topEnd,
                            child: InkWell(
                              child: Container(
                                padding: EdgeInsetsDirectional.all(10.h),
                                child: SvgPicture.asset(AppIcons.filledHeart),
                              ),
                              onTap: () async {},
                            ),
                          ),
                          Align(
                            alignment: AlignmentDirectional.topStart,
                            child: Padding(
                              padding: EdgeInsetsDirectional.only(top: 8.h),
                              child: Container(
                                height: 37.h,
                                width: 42.w,
                                decoration: BoxDecoration(
                                  color: AppColors.brown,
                                  borderRadius: BorderRadiusDirectional.only(
                                    topEnd: Radius.circular(6.r),
                                    bottomEnd: Radius.circular(6.r),
                                  ),
                                ),
                                child: Center(
                                  child: TextWidget(
                                    text: '${item.offer?.percentage ?? 0}%',
                                    color: AppColors.white,
                                    size: 12.sp,
                                    fontWeight: AppFonts.medium,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      // Provider.of<MainPageProvider>(context, listen: false).setScreen(
                      //   newScreen: OfferPage(offer: item),
                      //   title: item.title ?? '',
                      //   backScreen: const HomePage(),
                      // );
                    },
                  );
                },
              ),
            ],
          );
  }
}
