import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/bookmarks/models/Bookmark.dart';
import 'package:kafaat_loyalty/screens/bookmarks/providers/BookmarksProvider.dart';
import 'package:kafaat_loyalty/screens/bookmarks/ui/widgets/DiscountBookmarkItem.dart';
import 'package:kafaat_loyalty/screens/bookmarks/ui/widgets/OfferBookmarkItem.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestDiscounts/LatestDiscountsWidget.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestOffers/LatestOfferItem.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestOffers/LatestOffersWidget.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/myGiftCards/MyGiftCards.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/searchWidget/SearchTextField.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/LoadingState.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BookmarksPage extends StatelessWidget {
  const BookmarksPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BookmarksScreen();
  }
}

class BookmarksScreen extends StatelessWidget {
  BookmarksScreen({Key? key}) : super(key: key);
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
        child: Consumer<BookmarksProvider>(
          builder: (context, provider, child) {
            if (provider.bookmarks == null || provider.bookmarks?.response is LoadingState) {
              if (provider.bookmarks?.response is! LoadingState) provider.getBookmarks();
              return SizedBox(
                height: ScreenUtil.defaultSize.height - 100,
                width: double.infinity,
                child: const Center(child: CircularProgressIndicator()),
              );
            } else if (provider.bookmarks?.response is ApiException) {
              return SizedBox(
                height: 150.h,
                child: Center(child: TextWidget(text: provider.bookmarks?.response.businessError)),
              );
            } else if (provider.bookmarks?.statusCode == 200 && provider.bookmarks?.response is List<Bookmark>) {
              return Column(
                children: [
                  SearchTextField(searchController: searchController),
                  SizedBox(height: 45.h),
                  if (provider.offersBookmarks.isNotEmpty)
                    SizedBox(
                      height: 230.h,
                      child: provider.offersBookmarks.isEmpty
                          ? SizedBox(
                              height: 150.h,
                              child: Center(child: TextWidget(text: AppStrings.noItems)),
                            )
                          : Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 22.w),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          TextWidget(
                                              text: AppStrings.latestOffers,
                                              color: AppColors.black,
                                              size: 16.sp,
                                              fontWeight: AppFonts.medium),
                                          SizedBox(height: 9.h),
                                          TextWidget(
                                            text: AppStrings.checkTheLatestAddedOffers,
                                            color: AppColors.grey8F,
                                            size: 14.sp,
                                            fontWeight: AppFonts.regular,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 19.h),
                                Expanded(
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: provider.offersBookmarks.length,
                                    itemBuilder: (context, position) => OfferBookmarkItem(
                                      item: provider.offersBookmarks[position],
                                      backScreen: const BookmarksPage(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                    ),
                  SizedBox(height: 35.h),
                  DiscountBookmarkItem(items: provider.discountsBookmarks),
                ],
              );
            }
            return SizedBox(
              height: ScreenUtil.defaultSize.height - 100,
              width: double.infinity,
              child: const Center(child: CircularProgressIndicator()),
            );
          },
        ),
      ),
    );
  }
}
