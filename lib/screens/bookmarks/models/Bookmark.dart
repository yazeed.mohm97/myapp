import 'dart:convert';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

List<Bookmark> bookmarksFromJson(String str) => List<Bookmark>.from(json.decode(str).map((x) => Bookmark.fromJson(x)));

String bookmarksToJson(List<Bookmark> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Bookmark {
  Bookmark({
    this.id,
    this.offer,
    this.business,
  });

  int? id;
  Offer? offer;
  Business? business;

  factory Bookmark.fromJson(Map<String, dynamic> json) => Bookmark(
        id: json["id"],
        offer: Offer.fromJson(json["offer"]),
        business: Business.fromJson(json["business"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "offer": offer?.toJson(),
        "business": business?.toJson(),
      };
}

class Business {
  Business({
    this.id,
    this.name,
    this.enabled,
    this.qrCode,
    this.website,
    this.category,
    this.logo,
    this.locations,
    this.offers,
  });

  int? id;
  String? name;
  bool? enabled;
  String? qrCode;
  String? website;
  Category? category;
  dynamic logo;
  List<Location>? locations;
  List<Offer>? offers;

  factory Business.fromJson(Map<String, dynamic> json) => Business(
        id: json["id"],
        name: json["name"],
        enabled: json["enabled"],
        qrCode: json["qrCode"],
        website: json["website"],
        category: Category.fromJson(json["category"]),
        logo: json["logo"],
        locations: List<Location>.from(json["locations"].map((x) => Location.fromJson(x))),
        offers: List<Offer>.from(json["offers"].map((x) => Offer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "enabled": enabled,
        "qrCode": qrCode,
        "website": website,
        "category": category?.toJson(),
        "logo": logo,
        "locations": List<dynamic>.from(locations!.map((x) => x.toJson())),
        "offers": List<dynamic>.from(offers!.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    this.id,
    this.name,
  });

  int? id;
  String? name;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}

class Location {
  Location({
    this.id,
    this.latitude,
    this.longitude,
    this.name,
    this.generateQr,
    this.qrCode,
  });

  int? id;
  String? latitude;
  String? longitude;
  String? name;
  bool? generateQr;
  dynamic qrCode;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        name: json["name"],
        generateQr: json["generateQR"],
        qrCode: json["qrCode"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "name": name,
        "generateQR": generateQr,
        "qrCode": qrCode,
      };
}

class Offer {
  Offer({
    this.id,
    this.title,
    this.type,
    this.description,
    this.expirationDate,
    this.enabled,
    this.bookedMarked,
    this.bookedMarkId,
    this.qr,
    this.code,
    this.percentage,
    this.agreement,
    this.image,
    this.details,
    this.contact,
    this.businessName,
    this.locations,
  });

  int? id;
  String? title;
  String? type;
  String? description;
  DateTime? expirationDate;
  bool? enabled;
  dynamic bookedMarked;
  dynamic bookedMarkId;
  String? qr;
  String? code;
  double? percentage;
  Agreement? agreement;
  Agreement? image;
  Agreement? details;
  Contact? contact;
  dynamic businessName;
  List<dynamic>? locations;

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
        id: json["id"],
        title: json["title"],
        type: json["type"],
        description: json["description"],
        expirationDate: json["expirationDate"] == null ? null : DateTime.parse(json["expirationDate"]),
        enabled: json["enabled"],
        bookedMarked: json["bookedMarked"],
        bookedMarkId: json["bookedMarkId"],
        qr: json["qr"],
        code: json["code"],
        percentage: json["percentage"],
        agreement: json["agreement"] != null ? Agreement.fromJson(json["agreement"]) : null,
        image: Agreement.fromJson(json["image"]),
        details: json["details"] == null ? null : Agreement.fromJson(json["details"]),
        contact: json["contact"] != null ? Contact.fromJson(json["contact"]) : null,
        businessName: json["businessName"],
        locations: List<dynamic>.from(json["locations"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "type": type,
        "description": description,
        "expirationDate":
            "${expirationDate!.year.toString().padLeft(4, '0')}-${expirationDate!.month.toString().padLeft(2, '0')}-${expirationDate!.day.toString().padLeft(2, '0')}",
        "enabled": enabled,
        "bookedMarked": bookedMarked,
        "bookedMarkId": bookedMarkId,
        "qr": qr,
        "code": code,
        "percentage": percentage,
        "agreement": agreement?.toJson(),
        "image": image?.toJson(),
        "details": details?.toJson(),
        "contact": contact?.toJson(),
        "businessName": businessName,
        "locations": List<dynamic>.from(locations!.map((x) => x)),
      };
}

class Agreement {
  Agreement({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Agreement.fromJson(Map<String, dynamic> json) => Agreement(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}

class Contact {
  Contact({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phoneNumber,
  });

  int? id;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;

  factory Contact.fromJson(Map<String, dynamic> json) => Contact(
        id: json["id"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        email: json["email"],
        phoneNumber: json["phoneNumber"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "phoneNumber": phoneNumber,
      };
}
