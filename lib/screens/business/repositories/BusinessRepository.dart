import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BusinessRepository {
  Future<ApiResponseHandler> getBusiness(int businessId) async {
    return await httpClient.get(endPoint: 'businesses/$businessId');
  }
}
