import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kafaat_loyalty/screens/business/models/Business.dart';
import 'package:kafaat_loyalty/screens/business/providers/BusinessProvider.dart';
import 'package:kafaat_loyalty/screens/homePage/ui/widgets/latestOffers/LatestOfferItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/models/MainPageItem.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/offer/OfferPage.dart';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:provider/provider.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BusinessPage extends StatelessWidget {
  const BusinessPage({Key? key, required this.businessId}) : super(key: key);
  final int businessId;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => BusinessProvider(),
      child: BusinessScreen(businessId: businessId, parent: this),
    );
  }
}

class BusinessScreen extends StatelessWidget {
  const BusinessScreen({Key? key, required this.businessId, required this.parent}) : super(key: key);
  final int businessId;
  final Widget parent;

  @override
  Widget build(BuildContext context) {
    return Consumer<BusinessProvider>(
      builder: (context, provider, child) {
        if (provider.business == null) {
          provider.getBusiness(businessId);
          return const Center(child: CircularProgressIndicator(color: AppColors.brown));
        } else if (provider.business?.statusCode != 200) {
          return Center(
            child: TextWidget(text: AppStrings.errorOccurred),
          );
        } else if (provider.business?.statusCode == 200 && provider.business?.response is Business) {
          Business business = provider.business?.response;
          List<Offer> offers = <Offer>[];
          if (business.offers != null) offers = List.from(business.offers!.where((e) => e.type == 'OFFER').toList());

          List<Offer> discounts = <Offer>[];
          if (business.offers != null) discounts = List.from(business.offers!.where((e) => e.type == 'DISCOUNT').toList());

          return Column(
            children: [
              if (offers.isNotEmpty)
                SizedBox(
                  height: 223.h,
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 22.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextWidget(
                                    text: AppStrings.latestOffers, color: AppColors.black, size: 16.sp, fontWeight: AppFonts.medium),
                                SizedBox(height: 9.h),
                                TextWidget(
                                  text: AppStrings.checkTheLatestAddedOffers,
                                  color: AppColors.grey8F,
                                  size: 14.sp,
                                  fontWeight: AppFonts.regular,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 19.h),
                      Expanded(
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: offers.length,
                          itemBuilder: (context, position) => LatestOfferItem(
                            item: business.offers![position],
                            backScreen: parent,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              SizedBox(height: 35.h),
              if (discounts.isNotEmpty)
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 22.w),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextWidget(
                                  text: AppStrings.latestDiscounts, color: AppColors.black, size: 16.sp, fontWeight: AppFonts.medium),
                              SizedBox(height: 9.h),
                              TextWidget(
                                text: AppStrings.checkTheLatestAddedDiscounts,
                                color: AppColors.grey8F,
                                size: 14.sp,
                                fontWeight: AppFonts.regular,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 21.h),
                    GridView.builder(
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: discounts.length,
                      itemBuilder: (context, position) {
                        Offer item = discounts[position];
                        return InkWell(
                          child: Container(
                            height: 180.h,
                            width: 180.w,
                            margin: const EdgeInsets.all(4),
                            decoration: const BoxDecoration(
                              color: AppColors.white,
                              boxShadow: [BoxShadow(color: AppColors.grey0D, blurRadius: 24)],
                            ),
                            child: Stack(
                              children: [
                                Stack(
                                  children: [
                                    item.image?.filePath != null
                                        ? ClipRRect(
                                            borderRadius: BorderRadius.circular(6.r),
                                            child: CachedNetworkImage(
                                              imageUrl: item.image?.filePath ?? '',
                                              placeholder: (context, _) => SvgPicture.asset(
                                                AppIcons.image,
                                                color: AppColors.black,
                                                height: 75.h,
                                              ),
                                              height: double.infinity,
                                              fit: BoxFit.fill,
                                            ),
                                          )
                                        : Center(child: SvgPicture.asset(AppIcons.image, color: AppColors.black, height: 75.h)),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: TextWidget(text: item.title ?? ''),
                                    )
                                  ],
                                ),
                                Align(
                                  alignment: AlignmentDirectional.topEnd,
                                  child: InkWell(
                                    child: Container(
                                      padding: EdgeInsetsDirectional.all(10.h),
                                      child: SvgPicture.asset(item.bookedMarked ?? false ? AppIcons.filledHeart : AppIcons.heart),
                                    ),
                                    onTap: () async {
                                      // if (!item.bookedMarked!) {
                                      //   provider.bookmark(item.id!, true);
                                      //   var aResponse = await bookmarksProvider.addBookMark(item.id!);
                                      //   provider.bookmark(item.id!, aResponse?.statusCode == HttpStatus.created);
                                      //   bookmarksProvider.getBookmarks();
                                      // } else {
                                      //   provider.bookmark(item.id!, false);
                                      //   var dResponse = await bookmarksProvider.deleteBookmarkByOfferId(item.id!);
                                      //   provider.bookmark(item.id!, dResponse?.statusCode == HttpStatus.noContent ? false : true);
                                      //   bookmarksProvider.getBookmarks();
                                      // }
                                    },
                                  ),
                                ),
                                Align(
                                  alignment: AlignmentDirectional.topStart,
                                  child: Padding(
                                    padding: EdgeInsetsDirectional.only(top: 8.h),
                                    child: Container(
                                      height: 37.h,
                                      width: 42.w,
                                      decoration: BoxDecoration(
                                        color: AppColors.brown,
                                        borderRadius: BorderRadiusDirectional.only(
                                          topEnd: Radius.circular(6.r),
                                          bottomEnd: Radius.circular(6.r),
                                        ),
                                      ),
                                      child: Center(
                                        child: TextWidget(
                                          text: '${item.percentage}%',
                                          color: AppColors.white,
                                          size: 12.sp,
                                          fontWeight: AppFonts.medium,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () => Provider.of<MainPageProvider>(context, listen: false).navigate(
                            MainPageItem(-1, item.title ?? '', AppIcons.image, OfferPage(offer: item), false),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              if (offers.isEmpty && discounts.isEmpty)
                SizedBox(
                  height: MediaQuery.of(context).size.height - (kToolbarHeight * 5),
                  child: Center(
                    child: TextWidget(text: AppStrings.noItems),
                  ),
                )
            ],
          );
        }
        return const Center(child: CircularProgressIndicator(color: AppColors.brown));
      },
    );
  }
}
