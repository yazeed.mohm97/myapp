import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/screens/business/models/Business.dart';
import 'package:kafaat_loyalty/screens/business/repositories/BusinessRepository.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class BusinessProvider extends ChangeNotifier {
  ApiResponseHandler? _business;

  ApiResponseHandler? get business => _business;

  getBusiness(int businessId) async {
    _business = await BusinessRepository().getBusiness(businessId);
    if (_business?.statusCode == 200) {
      _business?.response = businessFromJson(_business?.response);
    }
    notifyListeners();
  }
}
