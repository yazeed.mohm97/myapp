import 'dart:convert';
import 'package:kafaat_loyalty/screens/offers/models/Offers.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

Business businessFromJson(String str) => Business.fromJson(json.decode(str));

String businessToJson(Business data) => json.encode(data.toJson());

class Business {
  Business({
    this.id,
    this.name,
    this.nameEn,
    this.enabled,
    this.qrCode,
    this.website,
    this.category,
    this.logo,
    this.locations,
    this.offers,
  });

  int? id;
  String? name;
  dynamic nameEn;
  bool? enabled;
  String? qrCode;
  dynamic website;
  Category? category;
  Logo? logo;
  List<Location>? locations;
  List<Offer>? offers;

  factory Business.fromJson(Map<String, dynamic> json) => Business(
        id: json["id"],
        name: json["name"],
        nameEn: json["nameEn"],
        enabled: json["enabled"],
        qrCode: json["qrCode"],
        website: json["website"],
        category: json["category"] == null ? null : Category.fromJson(json["category"]),
        logo: json["logo"] == null ? null : Logo.fromJson(json["logo"]),
        locations: json["locations"] == null ? [] : List<Location>.from(json["locations"]!.map((x) => Location.fromJson(x))),
        offers: json["offers"] == null ? [] : List<Offer>.from(json["offers"]!.map((x) => Offer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nameEn": nameEn,
        "enabled": enabled,
        "qrCode": qrCode,
        "website": website,
        "category": category?.toJson(),
        "logo": logo?.toJson(),
        "locations": locations == null ? [] : List<dynamic>.from(locations!.map((x) => x.toJson())),
        "offers": offers == null ? [] : List<dynamic>.from(offers!.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    this.id,
    this.name,
    this.nameEn,
  });

  int? id;
  String? name;
  dynamic nameEn;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        nameEn: json["nameEn"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nameEn": nameEn,
      };
}

class Location {
  Location({
    this.id,
    this.latitude,
    this.longitude,
    this.name,
    this.nameEn,
    this.generateQr,
    this.qrCode,
    this.businessName,
    this.businessId,
  });

  int? id;
  String? latitude;
  String? longitude;
  String? name;
  dynamic nameEn;
  bool? generateQr;
  dynamic qrCode;
  dynamic businessName;
  dynamic businessId;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json["id"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        name: json["name"],
        nameEn: json["nameEn"],
        generateQr: json["generateQR"],
        qrCode: json["qrCode"],
        businessName: json["businessName"],
        businessId: json["businessId"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "latitude": latitude,
        "longitude": longitude,
        "name": name,
        "nameEn": nameEn,
        "generateQR": generateQr,
        "qrCode": qrCode,
        "businessName": businessName,
        "businessId": businessId,
      };
}

class Logo {
  Logo({
    this.id,
    this.filePath,
    this.fileName,
    this.uuid,
  });

  int? id;
  String? filePath;
  String? fileName;
  String? uuid;

  factory Logo.fromJson(Map<String, dynamic> json) => Logo(
        id: json["id"],
        filePath: json["filePath"],
        fileName: json["fileName"],
        uuid: json["uuid"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filePath": filePath,
        "fileName": fileName,
        "uuid": uuid,
      };
}
