import 'dart:convert';
import 'dart:io';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class RegisterRepository {
  Future<ApiResponseHandler> register(Map<String, String> user) async {
    var response = await HttpClient().post(endPoint: 'register', payload: user);
    return response;
  }
}
