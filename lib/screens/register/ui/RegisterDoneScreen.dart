import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kafaat_loyalty/screens/login/ui/LoginPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/constants/AppFonts.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/ButtonWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class RegisterDoneScreen extends StatelessWidget {
  const RegisterDoneScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 51.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(width: double.infinity),
            SvgPicture.asset('assets/images/done.svg'),
            TextWidget(text: AppStrings.accountCreated, color: AppColors.brown, size: 26.sp, fontWeight: AppFonts.medium),
            SizedBox(height: 13.h),
            TextWidget(text: AppStrings.accountHasBeenCreated, size: 14.sp, fontWeight: AppFonts.regular, align: TextAlign.center),
            const Spacer(),
            ButtonWidget(
              onTap: () => nService.pushWithBack(const LoginPage(), context: context),
              text: AppStrings.continueT,
              radius: 8.r,
            ),
            SizedBox(height: 51.h),
          ],
        ),
      ),
    );
  }
}
