import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kafaat_loyalty/screens/onBoarding/ui/OnBoardingPage.dart';
import 'package:kafaat_loyalty/screens/otp/ui/OTPPage.dart';
import 'package:kafaat_loyalty/screens/register/repositories/RegisterRepository.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/WidgetsUtils.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiException.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const RegisterScreen();
  }
}

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  late LoadingButtonProvider _buttonProvider;
  late TextEditingController firstNameController;
  late TextEditingController lastNameController;
  late TextEditingController emailController;
  late TextEditingController phoneController;
  late TextEditingController passwordController;
  bool _showPassword = false;

  @override
  void initState() {
    firstNameController = TextEditingController();
    lastNameController = TextEditingController();
    emailController = TextEditingController();
    phoneController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    firstNameController.dispose();
    lastNameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      resizeToAvoidBottomInset: true,
      onBack: () => Navigator.of(context).pop(),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // SvgPicture.asset('assets/images/login.svg', height: 214.h, width: 232.89.w),
            SizedBox(height: 50.h),
            TextWidget(text: AppStrings.newAccount, color: AppColors.brown, size: 26.sp),
            SizedBox(height: 15.h),
            TextWidget(text: AppStrings.toRegister, color: AppColors.grey8F, size: 14.sp),
            SizedBox(height: 55.h),
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 40.w),
                child: Column(
                  children: [
                    ///name
                    TextFormField(
                      controller: firstNameController,
                      validator: (value) => (value == null || value.isEmpty) ? AppStrings.pleaseEnterYourFirstname : null,
                      decoration: InputDecoration(
                        prefixIcon: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SvgPicture.asset(AppIcons.profile, color: AppColors.greyBF, height: 18.04.h, width: 19.w),
                        ),
                        hintText: AppStrings.firstname,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIconConstraints: BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                      ),
                    ),
                    SizedBox(height: 15.h),
                    TextFormField(
                      controller: lastNameController,
                      validator: (value) => (value == null || value.isEmpty) ? AppStrings.pleaseEnterYourLastname : null,
                      decoration: InputDecoration(
                        prefixIcon: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SvgPicture.asset(AppIcons.profile, color: AppColors.greyBF, height: 18.04.h, width: 19.w),
                        ),
                        hintText: AppStrings.lastname,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIconConstraints: BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                      ),
                    ),
                    SizedBox(height: 15.h),

                    ///email
                    TextFormField(
                      controller: emailController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return AppStrings.pleaseEnterYourMail;
                        } else if (!isEmail(value)) {
                          return AppStrings.pleaseEnterValidMail;
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: AppStrings.companyEmail,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SvgPicture.asset(AppIcons.send, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                      ),
                    ),
                    SizedBox(height: 15.h),

                    ///phone
                    TextFormField(
                      controller: phoneController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return AppStrings.pleaseEnterYourPhone;
                        } else if (value.substring(0, 2) != '05' || value.length != 10) {
                          return AppStrings.pleaseEnterValidPhone;
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      maxLength: 10,
                      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                        counter: const SizedBox.shrink(),
                        hintText: AppStrings.phoneNumber,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SvgPicture.asset(AppIcons.call, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                      ),
                    ),
                    SizedBox(height: 15.h),

                    ///password
                    TextFormField(
                      controller: passwordController,
                      obscureText: !_showPassword,
                      validator: (value) => (value == null || value.isEmpty) ? AppStrings.pleaseEnterYourPassword : null,
                      decoration: InputDecoration(
                        hintText: AppStrings.password,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        ),
                        suffixIcon: InkWell(
                          child: SvgPicture.asset(_showPassword ? AppIcons.hide : AppIcons.show,
                              color: AppColors.greyBF, height: 15.h, width: 19.w),
                          onTap: () => setState(() => _showPassword = !_showPassword),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                        suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                      ),
                    ),

                    ///register button
                    LoadingButton(
                      text: AppStrings.nextStep,
                      radius: 8.r,
                      margin: EdgeInsets.only(top: 100.h, bottom: 29.h),
                      usedProvider: (provider) => _buttonProvider = provider,
                      onTap: () async {
                        if (_formKey.currentState!.validate()) {
                          _buttonProvider.setLoading(true);
                          var response = await RegisterRepository().register(getUser());
                          _buttonProvider.setLoading(false);
                          if (response.statusCode == HttpStatus.created) {
                            if (mounted) nService.pushWithBack(OTPPage(email: emailController.text), context: context);
                          } else if (response.response is ApiException) {
                            if (mounted) WidgetsUtils.snackBar(context, response.response.businessError, bgColor: AppColors.red);
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Map<String, String> getUser() => {
        'firstName': firstNameController.text,
        'lastName': lastNameController.text,
        'email': emailController.text,
        'phoneNumber': phoneController.text,
        'password': passwordController.text,
        'login': emailController.text,
        'langKey': context.locale.languageCode.toLowerCase() == 'ar' ? "ar-ly" : 'en',
      };

  bool isEmail(String email) {
    var reg = RegExp(r"^[a-zA-Z\d.a-zA-Z\d.\-_]+@[a-zA-Z\d.\-_]+\.[a-zA-Z]+");
    return reg.hasMatch(email);
  }
}
