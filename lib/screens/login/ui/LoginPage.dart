import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kafaat_loyalty/screens/forgetPassword/ui/ForgetPasswordPage.dart';
import 'package:kafaat_loyalty/screens/login/providers/LoginController.dart';
import 'package:kafaat_loyalty/screens/mainPage/ui/MainPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/VariablesUtils.dart';
import 'package:kafaat_loyalty/utils/WidgetsUtils.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key, this.email}) : super(key: key);
  final String? email;

  @override
  Widget build(BuildContext context) {
    return const LoginScreen();
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key, this.email}) : super(key: key);
  final String? email;

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoadingButtonProvider? _provider;

  final _formKey = GlobalKey<FormState>();
  late TextEditingController emailController;
  late TextEditingController passwordController;

  bool _showPassword = false;

  @override
  void initState() {
    emailController = TextEditingController(text: widget.email);
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      resizeToAvoidBottomInset: true,
      onBack: () => Navigator.of(context).pop(),
      child: SingleChildScrollView(
        child: SizedBox(
          height: MediaQuery.of(context).size.height - (kToolbarHeight * 2),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // SvgPicture.asset('assets/images/login.svg', height: 214.h, width: 232.89.w),
              SizedBox(height: 100.h),
              TextWidget(text: AppStrings.accountLogin, color: AppColors.brown, size: 26.sp),
              SizedBox(height: 13.h),
              TextWidget(text: AppStrings.toLogin, color: AppColors.grey8F, size: 14.sp),
              SizedBox(height: 80.h),
              Form(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40.w),
                  child: Column(
                    children: [
                      TextFormField(
                        controller: emailController,
                        validator: (value) {
                          if (value == null || value.isEmpty) return AppStrings.pleaseEnterYourMail;
                          return null;
                        },
                        decoration: InputDecoration(
                          prefixIcon: Align(
                            alignment: AlignmentDirectional.centerStart,
                            child: SvgPicture.asset(AppIcons.send, color: AppColors.greyBF, height: 18.04.h, width: 19.w),
                          ),
                          hintText: AppStrings.companyEmail,
                          hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                          prefixIconConstraints:
                              BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                        ),
                      ),
                      SizedBox(height: 30.h),
                      TextFormField(
                        controller: passwordController,
                        obscureText: !_showPassword,
                        validator: (value) {
                          if (value == null || value.isEmpty) AppStrings.pleaseEnterYourPassword;
                          return null;
                        },
                        decoration: InputDecoration(
                          hintText: AppStrings.password,
                          hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                          prefixIcon: Align(
                            alignment: AlignmentDirectional.centerStart,
                            child: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                          ),
                          suffixIcon: InkWell(
                            child: SvgPicture.asset(_showPassword ? AppIcons.hide : AppIcons.show,
                                color: AppColors.greyBF, height: 15.h, width: 19.w),
                            onTap: () => setState(() => _showPassword = !_showPassword),
                          ),
                          prefixIconConstraints:
                              BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                          suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                        ),
                      ),
                      SizedBox(height: 42.h),
                      Align(
                        alignment: AlignmentDirectional.centerEnd,
                        child: InkWell(
                          child: TextWidget(text: AppStrings.forgetPassword, color: AppColors.brown),
                          onTap: () => nService.pushWithBack(context: context, const ForgetPasswordPage()),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              LoadingButton(
                onTap: () async {
                  if (_formKey.currentState!.validate()) {
                    _provider?.setLoading(true);
                    ApiResponseHandler? response = await LoginController().login(emailController.text, passwordController.text);
                    _provider?.setLoading(false);
                    if (response?.statusCode == 200) {
                      VariablesUtils.userMail = emailController.text;
                      nService.pushAndRemove(const MainPage());
                    } else {
                      if (mounted) {
                        WidgetsUtils.snackBar(
                          context,
                          response?.response.businessError ?? AppStrings.unexpectedError,
                          bgColor: AppColors.red,
                        );
                      }
                    }
                  }
                },
                text: AppStrings.login,
                radius: 8.r,
                margin: EdgeInsets.only(bottom: 60.h),
                usedProvider: (provider) => _provider = provider,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
