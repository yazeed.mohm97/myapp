import 'package:kafaat_loyalty/screens/login/models/Login.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/data/ApiExceptionsData.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class LoginRepository {
  Future login(Map<String, dynamic> user) async {
    var response = await HttpClient().post(endPoint: 'authenticate', payload: user);
    if (response.statusCode == 200) {
      response.response = loginFromJson(response.response);
    }
    return response;
  }
}
