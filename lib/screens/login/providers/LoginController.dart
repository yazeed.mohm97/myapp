import 'package:kafaat_loyalty/screens/login/models/Login.dart';
import 'package:kafaat_loyalty/screens/login/repositories/LoginRepository.dart';
import 'package:kafaat_loyalty/utils/VariablesUtils.dart';
import 'package:kafaat_loyalty/utils/cahce/CacheManager.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class LoginController {
  Future<ApiResponseHandler?> login(String email, String password) async {
    try {
      ApiResponseHandler response = await LoginRepository().login({"password": password, "rememberMe": true, "username": email});
      if (response.statusCode == 200 && response.response is Login) {
        UserCache.setToken(response.response.idToken);
        VariablesUtils.headers['Authorization'] = 'Bearer ${response.response.idToken}';
      }
      return response;
    } catch (ex) {
      return null;
    }
  }
}
