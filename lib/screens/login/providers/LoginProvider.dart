import 'package:flutter/material.dart';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class LoginProvider extends ChangeNotifier {
  ApiResponseHandler? _response;

  ApiResponseHandler? get response => _response;

  login(Map<String, String> credentials) async {
    try {
      _response = await HttpClient().post(endPoint: 'authenticate');
      notifyListeners();
    } catch (e) {
      notifyListeners();
    }
  }
}
