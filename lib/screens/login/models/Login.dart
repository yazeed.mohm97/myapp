import 'dart:convert';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {
  Login({
    this.idToken,
  });

  String? idToken;

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        idToken: json["id_token"],
      );

  Map<String, dynamic> toJson() => {
        "id_token": idToken,
      };
}
