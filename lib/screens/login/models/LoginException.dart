import 'dart:convert';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

LoginException loginExceptionFromJson(String str) => LoginException.fromJson(json.decode(str));

String loginExceptionToJson(LoginException data) => json.encode(data.toJson());

class LoginException {
  LoginException({
    this.type,
    this.title,
    this.status,
    this.detail,
    this.path,
    this.message,
  });

  String? type;
  String? title;
  int? status;
  String? detail;
  String? path;
  String? message;

  factory LoginException.fromJson(Map<String, dynamic> json) => LoginException(
        type: json["type"],
        title: json["title"],
        status: json["status"],
        detail: json["detail"],
        path: json["path"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "title": title,
        "status": status,
        "detail": detail,
        "path": path,
        "message": message,
      };
}
