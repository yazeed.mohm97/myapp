import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kafaat_loyalty/screens/forgetPassword/repositories/ForgetPasswordRepository.dart';
import 'package:kafaat_loyalty/screens/login/ui/LoginPage.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppIcons.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/widgets/BackgroundWidget.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/providers/LoadingButtonProvider.dart';
import 'package:kafaat_loyalty/utils/widgets/buttons/loadingButton/ui/LoadingButton.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ForgetPasswordPage extends StatelessWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const ForgetPasswordScreen();
  }
}

class ForgetPasswordScreen extends StatefulWidget {
  const ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgetPasswordScreen> createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  late LoadingButtonProvider _buttonProvider;
  final _formKey = GlobalKey<FormState>();

  late TextEditingController email;
  late TextEditingController otp;
  late TextEditingController newPassword;
  late TextEditingController repeatPassword;
  bool _showPassword = false;

  bool _otpChecked = false;
  bool _otpSent = false;

  @override
  void initState() {
    email = TextEditingController();
    otp = TextEditingController();
    newPassword = TextEditingController();
    repeatPassword = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    email.dispose();
    otp.dispose();
    newPassword.dispose();
    repeatPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      resizeToAvoidBottomInset: true,
      onBack: () => Navigator.of(context).pop(),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset('assets/images/login.svg', height: 214.h, width: 232.89.w),
            SizedBox(height: 30.h),
            TextWidget(text: AppStrings.forgetPassword, color: AppColors.brown, size: 26.sp),
            SizedBox(height: 10.h),
            TextWidget(text: AppStrings.toChangePassword, color: AppColors.grey8F, size: 14.sp),
            SizedBox(height: 50.h),
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 40.w),
                child: Column(
                  children: [
                    ///email
                    TextFormField(
                      controller: email,
                      validator: (value) {
                        if (value == null || value.isEmpty) AppStrings.pleaseEnterYourMail;
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: AppStrings.companyEmail,
                        hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                        prefixIcon: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SvgPicture.asset(AppIcons.send, color: AppColors.greyBF, height: 17.h, width: 20.w),
                        ),
                        prefixIconConstraints: BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                      ),
                    ),
                    SizedBox(height: 30.h),

                    ///otp
                    if (_otpSent)
                      Column(
                        children: [
                          TextFormField(
                            controller: otp,
                            validator: (value) {
                              if (value == null || value.isEmpty) AppStrings.pleaseEnterYourPassword;
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: AppStrings.otpVerification,
                              hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                              prefixIcon: Align(
                                alignment: AlignmentDirectional.centerStart,
                                child: SvgPicture.asset(AppIcons.password, color: AppColors.greyBF, height: 17.h, width: 20.w),
                              ),
                              prefixIconConstraints:
                                  BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                            ),
                          ),
                          SizedBox(height: 30.h),
                        ],
                      ),

                    ///password
                    if (_otpChecked)
                      Column(
                        children: [
                          TextFormField(
                            controller: newPassword,
                            obscureText: !_showPassword,
                            validator: (value) {
                              if (value == null || value.isEmpty) AppStrings.pleaseEnterYourPassword;
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: AppStrings.newPassword,
                              hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                              prefixIcon: Align(
                                alignment: AlignmentDirectional.centerStart,
                                child: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                              ),
                              suffixIcon: InkWell(
                                child: SvgPicture.asset(_showPassword ? AppIcons.hide : AppIcons.show,
                                    color: AppColors.greyBF, height: 15.h, width: 19.w),
                                onTap: () => setState(() => _showPassword = !_showPassword),
                              ),
                              prefixIconConstraints:
                                  BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                              suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                            ),
                          ),
                          SizedBox(height: 30.h),
                          TextFormField(
                            controller: repeatPassword,
                            obscureText: !_showPassword,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return AppStrings.pleaseEnterYourPassword;
                              } else if (value != newPassword.text) {
                                return AppStrings.passwordDoesNotMatch;
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: AppStrings.repeatPassword,
                              hintStyle: TextStyle(color: AppColors.greyBF, fontSize: 16.sp),
                              prefixIcon: Align(
                                alignment: AlignmentDirectional.centerStart,
                                child: SvgPicture.asset(AppIcons.lock, color: AppColors.greyBF, height: 17.h, width: 20.w),
                              ),
                              suffixIcon: InkWell(
                                child: SvgPicture.asset(_showPassword ? AppIcons.hide : AppIcons.show,
                                    color: AppColors.greyBF, height: 15.h, width: 19.w),
                                onTap: () => setState(() => _showPassword = !_showPassword),
                              ),
                              prefixIconConstraints:
                                  BoxConstraints(maxHeight: 18.04.h, maxWidth: 35.w, minHeight: 18.04.h, minWidth: 35.w),
                              suffixIconConstraints: BoxConstraints(maxHeight: 15.h, maxWidth: 19.w, minHeight: 15.h, minWidth: 19.w),
                            ),
                          ),
                        ],
                      ),

                    ///button
                    LoadingButton(
                      text: _otpSent
                          ? _otpChecked
                              ? AppStrings.save
                              : AppStrings.nextStep
                          : AppStrings.nextStep,
                      radius: 8.r,
                      margin: EdgeInsets.only(top: 75.h, bottom: 29.h),
                      usedProvider: (provider) => _buttonProvider = provider,
                      onTap: () async {
                        _buttonProvider.setLoading(true);
                        if (_otpSent) {
                          if (_otpChecked) {
                            var finish = await ForgetPasswordRepository().finish(email.text, otp.text, newPassword.text);
                            if (finish) {
                              Fluttertoast.showToast(msg: 'msg');
                              nService.pushAndRemove(const LoginPage());
                            }
                          } else {
                            var check = await ForgetPasswordRepository().check(email.text, otp.text);
                            if (check) {
                              setState(() => _otpChecked = true);
                            } else {
                              Fluttertoast.showToast(msg: 'Wrong OTP');
                            }
                          }
                        } else {
                          var init = await ForgetPasswordRepository().init(email.text);
                          if (init) {
                            setState(() => _otpSent = true);
                          }
                        }

                        _buttonProvider.setLoading(false);
                      },
                    ),
                    // SizedBox(height: 40.h),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
