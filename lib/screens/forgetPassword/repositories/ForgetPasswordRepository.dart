import 'dart:io';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class ForgetPasswordRepository {
  Future<bool> init(String email) async {
    var response = await HttpClient().post(endPoint: 'account/reset-password/init', payload: email, isJson: false);
    return response.statusCode == HttpStatus.ok;
  }

  Future<bool> check(String email, String otp) async {
    var response = await HttpClient().post(endPoint: 'account/reset-password/check', payload: {'email': email, 'key': otp});
    return response.statusCode == HttpStatus.ok && response.response == 'true';
  }

  Future<bool> finish(String email, String key, String password) async {
    var response = await HttpClient().post(
      endPoint: 'account/reset-password/finish',
      payload: {'email': email, 'key': key, 'newPassword': password},
    );
    return response.statusCode == HttpStatus.ok;
  }
}
