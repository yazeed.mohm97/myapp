import 'package:easy_pdf_viewer/easy_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class PDFViewerScreen extends StatefulWidget {
  const PDFViewerScreen({Key? key, required this.filePath}) : super(key: key);
  final String filePath;

  @override
  State<PDFViewerScreen> createState() => _PDFViewerScreenState();
}

class _PDFViewerScreenState extends State<PDFViewerScreen> {
  late PDFDocument document;
  bool _isLoading = true;

  @override
  void initState() {
    getFile();
    super.initState();
  }

  getFile() async {
    document = await PDFDocument.fromURL(
      widget.filePath,
      cacheManager: CacheManager(
        Config(
          widget.filePath,
          stalePeriod: const Duration(days: 7),
          maxNrOfCacheObjects: 10,
        ),
      ),
    );

    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 8.h, horizontal: 16.w),
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(8.r),
            ),
            child: _isLoading
                ? const Center(child: CircularProgressIndicator())
                : ClipRRect(
                    borderRadius: BorderRadius.circular(8.r),
                    child: PDFViewer(
                      document: document,
                      lazyLoad: false,
                      scrollDirection: Axis.vertical,
                      showNavigation: false,
                      showPicker: false,
                      zoomSteps: 2,
                      backgroundColor: Colors.white,
                    ),
                  ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 16.h, bottom: 8.h),
          child: FloatingActionButton(
            backgroundColor: AppColors.white,
            elevation: 0,
            onPressed: () => Navigator.pop(context),
            child: const Icon(Icons.close, color: AppColors.grey8F),
          ),
        ),
      ],
    );
  }
}
