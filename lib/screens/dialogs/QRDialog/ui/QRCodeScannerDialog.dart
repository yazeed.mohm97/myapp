import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kafaat_loyalty/screens/dialogs/QRDialog/repositories/QRRepository.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class QRCodeScannerDialog extends StatefulWidget {
  const QRCodeScannerDialog({Key? key, required this.offerId}) : super(key: key);
  final int offerId;

  @override
  State<QRCodeScannerDialog> createState() => _QRCodeScannerDialogState();
}

class _QRCodeScannerDialogState extends State<QRCodeScannerDialog> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;
  String? code;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 71.h),
        TextWidget(text: AppStrings.scanQRCode, color: AppColors.white, size: 20),
        Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: 355.h,
              width: 298.w,
              margin: EdgeInsets.only(top: 109.h),
              child: Center(
                // child: QRView(
                //   key: qrKey,
                //   overlay: QrScannerOverlayShape(borderColor: AppColors.white, borderWidth: 5.w, cutOutSize: 355.h),
                //   onQRViewCreated: _onQRViewCreated,
                // ),
                child: _buildQrView(),
              ),
            ),
            if (code != null)
              Padding(
                padding: EdgeInsets.only(top: 109.h),
                child: Center(
                  child: Container(
                    alignment: Alignment.center,
                    height: 355.h,
                    width: 298.w,
                    color: AppColors.greyBF.withOpacity(0.75),
                    child:
                        // code == null
                        //     ? const CircularProgressIndicator(color: AppColors.brown)
                        //     :
                        TextWidget(text: code!, size: 32.sp, color: AppColors.black, fontWeight: FontWeight.w700),
                  ),
                ),
              ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 77.h),
          child: FloatingActionButton(
            backgroundColor: AppColors.white,
            elevation: 0,
            onPressed: () => Navigator.pop(context),
            child: const Icon(Icons.close, color: AppColors.grey8F),
          ),
        ),
      ],
    );
  }

  Widget _buildQrView() {
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
        borderColor: AppColors.white,
        borderWidth: 5.w,
        cutOutSize: 355.h,
      ),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() => this.controller = controller);
    try {
      controller.scannedDataStream.listen((scanData) async {
        await this.controller?.pauseCamera();

        var response = await QRRepository().scanQR(scanData.code!, widget.offerId);
        if (response.statusCode == HttpStatus.ok) {
          setState(() => code = response.response);
        } else {
          Fluttertoast.showToast(msg: AppStrings.errorOccurred);
        }
      });
    } catch (e) {
      Fluttertoast.showToast(msg: AppStrings.unexpectedError);
    }
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    if (!p) ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('no Permission')));
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
