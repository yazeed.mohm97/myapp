import 'dart:io';
import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';
import 'package:kafaat_loyalty/utils/httpClient/models/ApiResponseHandler.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class QRRepository {
  Future<ApiResponseHandler> scanQR(String content, int offerId) async {
    var response = await HttpClient().get(endPoint: 'scan-qr?content=$content&offerId=$offerId');
    return response;
  }
}
