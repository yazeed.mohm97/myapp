import 'package:kafaat_loyalty/utils/httpClient/HttpClient.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class DiscountRepository {
  Future increaseVisit(int offerId) async {
    await httpClient.post(endPoint: 'offer-logs', payload: "$offerId", isJson: false);
  }
}

DiscountRepository discountRepository = DiscountRepository();
