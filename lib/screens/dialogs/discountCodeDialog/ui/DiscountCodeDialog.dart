import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/screens/dialogs/discountCodeDialog/repositories/DiscountRepository.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/AppStrings.dart';
import 'package:kafaat_loyalty/utils/widgets/texts/TextWidget.dart';

/// created by: yazeed nasrullah
/// email: yazeed.mohm97@gmail.com

class DiscountCodeDialog extends StatelessWidget {
  const DiscountCodeDialog({
    Key? key,
    required this.offerId,
    required this.title,
    required this.subTitle,
    required this.offerNo,
    required this.amount,
    required this.code,
  }) : super(key: key);
  final int offerId;
  final String title;
  final String subTitle;
  final String offerNo;
  final String amount;
  final String code;

  @override
  Widget build(BuildContext context) {
    discountRepository.increaseVisit(offerId);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 327.w,
          margin: EdgeInsets.only(top: 68.h),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(8.r),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 33.h),
              TextWidget(text: title, size: 18.sp, color: AppColors.green7F),
              SizedBox(height: 2.h),
              TextWidget(text: subTitle, size: 13.sp, color: AppColors.grey8F),
              const Divider(),
              SizedBox(height: 18.h),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextWidget(text: AppStrings.offerNo, size: 15.sp, color: AppColors.grey8F),
                    TextWidget(text: offerNo, size: 14.sp, color: AppColors.green7F),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 18.h, horizontal: 24.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextWidget(text: AppStrings.amount, size: 15.sp, color: AppColors.grey8F),
                    TextWidget(text: amount, size: 14.sp, color: AppColors.green7F),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: 259.h,
          width: 327.w,
          margin: EdgeInsets.only(top: 25.h),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(8.r),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 73.h,
                width: 264.w,
                margin: EdgeInsets.only(bottom: 20.h),
                color: AppColors.greyBF.withOpacity(0.25),
                child: DottedBorder(
                  strokeWidth: 1,
                  color: AppColors.brown,
                  borderType: BorderType.RRect,
                  radius: Radius.circular(6.r),
                  dashPattern: const [7, 7],
                  child: Center(child: TextWidget(text: code, size: 32.sp, color: AppColors.green7F)),
                ),
              ),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  child: Padding(
                    padding: const EdgeInsets.all(13),
                    child: TextWidget(text: AppStrings.copyCode, size: 13.sp, color: AppColors.green7F, underline: true),
                  ),
                  onTap: () async => await Clipboard.setData(ClipboardData(text: code)),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 77.h),
          child: FloatingActionButton(
            backgroundColor: AppColors.white,
            elevation: 0,
            onPressed: () => Navigator.pop(context),
            child: const Icon(Icons.close, color: AppColors.grey8F),
          ),
        ),
      ],
    );
  }
}
