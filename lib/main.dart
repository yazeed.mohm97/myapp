import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kafaat_loyalty/firebase_options.dart';
import 'package:kafaat_loyalty/screens/bookmarks/providers/BookmarksProvider.dart';
import 'package:kafaat_loyalty/screens/business/providers/BusinessProvider.dart';
import 'package:kafaat_loyalty/screens/familyMembers/providers/FamilyMembersProvider.dart';
import 'package:kafaat_loyalty/screens/mainPage/providers/MainPageProvider.dart';
import 'package:kafaat_loyalty/screens/mainPage/ui/MainPage.dart';
import 'package:kafaat_loyalty/screens/myGiftCards/providers/MyGiftsProvider.dart';
import 'package:kafaat_loyalty/screens/offers/providers/OffersProvider.dart';
import 'package:kafaat_loyalty/screens/onBoarding/ui/OnBoardingPage.dart';
import 'package:kafaat_loyalty/screens/profile/providers/ProfileProvider.dart';
import 'package:kafaat_loyalty/utils/AppColors.dart';
import 'package:kafaat_loyalty/utils/NavigationService.dart';
import 'package:kafaat_loyalty/utils/VariablesUtils.dart';
import 'package:kafaat_loyalty/utils/cahce/CacheManager.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  VariablesUtils.prefs = await SharedPreferences.getInstance();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    systemNavigationBarColor: Colors.transparent,
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.dark,
  ));

  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('ar'), Locale('en')],
      path: 'assets/langs',
      fallbackLocale: const Locale('ar'),
      startLocale: Locale(Platform.localeName.substring(0, 2)),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    VariablesUtils.headers['Accept-Language'] = context.locale.languageCode.toLowerCase() == 'ar' ? 'ar-ly' : 'en';
    return ScreenUtilInit(
        designSize: const Size(428, 926),
        minTextAdapt: true,
        splitScreenMode: false,
        builder: (context, child) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => MainPageProvider()),
              ChangeNotifierProvider(create: (_) => MyGiftsProvider()),
              ChangeNotifierProvider(create: (_) => OffersProvider()),
              ChangeNotifierProvider(create: (_) => BookmarksProvider()),
              ChangeNotifierProvider(create: (_) => ProfileProvider()),
              ChangeNotifierProvider(create: (_) => FamilyMembersProvider()),
            ],
            child: Container(
              color: AppColors.white,
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                navigatorKey: NavigationService.navigatorKey,
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.locale,
                theme: ThemeData(
                  primaryColor: AppColors.brown,
                  scaffoldBackgroundColor: Colors.white,
                  fontFamily: 'ReadexPro',
                  appBarTheme: const AppBarTheme(elevation: 0, iconTheme: IconThemeData(color: AppColors.black)),
                ),
                home: destination(),
              ),
            ),
          );
        });
  }

  Widget destination() {
    if (UserCache.isTokenExist()) {
      return const MainPage();
    }
    return const OnBoardingPage();
  }
}
